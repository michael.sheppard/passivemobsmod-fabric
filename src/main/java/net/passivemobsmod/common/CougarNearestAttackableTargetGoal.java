/*
 * CougarNearestAttackableTargetGoal.java
 *
 *  Copyright (c) 2021 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.common;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.TargetPredicate;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.TrackTargetGoal;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.Box;

import java.util.EnumSet;
import java.util.function.Predicate;

@SuppressWarnings("unused")
public class CougarNearestAttackableTargetGoal<T extends LivingEntity> extends TrackTargetGoal {
    protected final Class<T> targetClass;
    protected final int reciprocalChance;
    protected LivingEntity targetEntity;
    protected TargetPredicate targetPredicate;

    public CougarNearestAttackableTargetGoal(MobEntity goalOwnerIn, Class<T> targetClassIn, boolean checkSight) {
        this(goalOwnerIn, targetClassIn, checkSight, false);
    }

    public CougarNearestAttackableTargetGoal(MobEntity goalOwnerIn, Class<T> targetClassIn, boolean checkSight, boolean nearbyOnlyIn) {
        this(goalOwnerIn, targetClassIn, 10, checkSight, nearbyOnlyIn, null);
    }

    public CougarNearestAttackableTargetGoal(MobEntity goalOwnerIn, Class<T> targetClassIn, int targetChanceIn, boolean checkSight, boolean nearbyOnlyIn, Predicate<LivingEntity> targetPredicate) {
        super(goalOwnerIn, checkSight, nearbyOnlyIn);
        targetClass = targetClassIn;
        reciprocalChance = targetChanceIn;
        setControls(EnumSet.of(Goal.Control.TARGET));
        this.targetPredicate = (new TargetPredicate()).setBaseMaxDistance(getFollowRange()).setPredicate(targetPredicate);
    }

    protected Box getSearchBox(double distance) {
        return mob.getBoundingBox().expand(distance, 4.0D, distance);
    }

    protected void findClosestTarget() {
        if (targetClass != PlayerEntity.class && targetClass != ServerPlayerEntity.class) {
            targetEntity = mob.world.getClosestEntityIncludingUngeneratedChunks(targetClass, targetPredicate, mob, mob.getX(), mob.getEyeY(), mob.getZ(), getSearchBox(getFollowRange()));
        } else {
            targetEntity = mob.world.getClosestPlayer(targetPredicate, mob, mob.getX(), mob.getEyeY(), mob.getZ());
        }

    }

    @Override
    public boolean canStart() {
        if (reciprocalChance > 0 && mob.getRandom().nextInt(reciprocalChance) != 0) {
            return false;
        } else {
            findClosestTarget();
            return targetEntity != null;
        }
    }

    public void start() {
        mob.setTarget(targetEntity);
        super.start();
    }

    public void setTargetEntity(LivingEntity targetEntity) {
        this.targetEntity = targetEntity;
    }
}
