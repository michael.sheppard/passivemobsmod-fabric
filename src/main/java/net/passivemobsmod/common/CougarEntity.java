/*
 * CougarEntity.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.common;

import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.passive.PigEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeKeys;

import java.util.Objects;
import java.util.Optional;
import java.util.Random;

public class CougarEntity extends AnimalEntity {

    public CougarEntity(EntityType<? extends CougarEntity> entityType, World world) {
        super(entityType, world);
    }

    protected void initGoals() {
        goalSelector.add(4, new PounceAtTargetGoal(this, 0.4F));
        goalSelector.add(5, new CougarEntity.AttackGoal());
        goalSelector.add(8, new WanderAroundFarGoal(this, 1.0D));
        goalSelector.add(10, new LookAtEntityGoal(this, PlayerEntity.class, 8.0F));
        goalSelector.add(10, new LookAroundGoal(this));
        targetSelector.add(3, (new RevengeGoal(this)).setGroupRevenge());
        targetSelector.add(4, new FollowTargetGoal<>(this, PlayerEntity.class, true, false));
        targetSelector.add(7, new FollowTargetGoal<>(this, PigEntity.class, false));
        targetSelector.add(5, new CougarNearestAttackableTargetGoal<>(this, PlayerEntity.class, true, true));
        targetSelector.add(6, new CougarNearestAttackableTargetGoal<>(this, PigEntity.class, false, true));
    }

    public static DefaultAttributeContainer.Builder createAttributes() {
        return MobEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MAX_HEALTH, 30.0D)
                .add(EntityAttributes.GENERIC_FOLLOW_RANGE, 20.0D)
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.25D)
                .add(EntityAttributes.GENERIC_ATTACK_DAMAGE, 6.0D);
    }

    // implementing Biome specific spawning here
    @SuppressWarnings("unused")
    public static boolean canSpawn(EntityType<CougarEntity> type, WorldAccess world, SpawnReason spawnReason, BlockPos pos, Random random) {
        return validSpawnBiomes(world, pos);
    }

    private static boolean validSpawnBiomes(WorldAccess world, BlockPos pos) {
        Optional<RegistryKey<Biome>> optional = world.method_31081(pos);
        return Objects.equals(optional, Optional.of(BiomeKeys.PLAINS)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.BIRCH_FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.FLOWER_FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.MOUNTAINS)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.TAIGA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.GIANT_SPRUCE_TAIGA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.SNOWY_TAIGA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.GIANT_TREE_TAIGA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.BADLANDS)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.BADLANDS_PLATEAU)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.MODIFIED_BADLANDS_PLATEAU)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.MODIFIED_WOODED_BADLANDS_PLATEAU)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.SNOWY_TUNDRA));
    }

    protected void initDataTracker() {
        super.initDataTracker();
//        dataTracker.startTracking(angerTime, 0);
    }


//    public boolean isWarning() {
//        return dataTracker.get(WARNING);
//    }


    @Override
    public EntityDimensions getDimensions(EntityPose p) {
        return new EntityDimensions(1.0f, 1.5f, false);
    }

    protected float getActiveEyeHeight(EntityPose pose, EntityDimensions dimensions) {
        return dimensions.height * 0.8F;
    }

    protected SoundEvent getAmbientSound() {
        return PassiveMobsMod.COUGAR_AMBIENT_SOUND;
    }

    protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
        return PassiveMobsMod.COUGAR_ANGRY_SOUND;
    }

    protected SoundEvent getDeathSound() {
        return PassiveMobsMod.COUGAR_ANGRY_SOUND;
    }

    protected void playWarningSound() {
            playSound(PassiveMobsMod.COUGAR_WARNING_SOUND, 1.0F, getSoundPitch());
    }

    protected float getSoundVolume() {
        return 0.4F;
    }

    public int getLimitPerChunk() {
        return 8;
    }

    public boolean damage(DamageSource source, float amount) {
        if (isInvulnerableTo(source)) {
            return false;
        } else {
            Entity entity = source.getAttacker();
            if (entity != null && !(entity instanceof PlayerEntity) && !(entity instanceof PersistentProjectileEntity)) {
                amount = (amount + 1.0F) / 2.0F;
            }

            return super.damage(source, amount);
        }
    }


    public boolean tryAttack(Entity target) {
        boolean bl = target.damage(DamageSource.mob(this), (float) ((int) getAttributeValue(EntityAttributes.GENERIC_ATTACK_DAMAGE)));
        if (bl) {
            dealDamage(this, target);
        }

        return bl;
    }

    @Override
    public PassiveEntity createChild(ServerWorld serverWorld, PassiveEntity passiveEntity) {
        return PassiveMobsMod.COUGAR.create(serverWorld);
    }

    class AttackGoal extends MeleeAttackGoal {
        public AttackGoal() {
            super(CougarEntity.this, 1.25D, true);
        }

        protected void attack(LivingEntity target, double squaredDistance) {
            double attackDistance = getSquaredMaxAttackDistance(target);
            if (squaredDistance <= attackDistance && method_28347()) {
                method_28346();
                mob.tryAttack(target);
            } else if (squaredDistance <= attackDistance * 2.0D) {
                if (method_28347()) {
                    method_28346();
                }

                if (method_28348() <= 10) {
                    CougarEntity.this.playWarningSound();
                }
            } else {
                method_28346();
            }

        }

        public void stop() {
            super.stop();
        }

        protected double getSquaredMaxAttackDistance(LivingEntity entity) {
            return 4.0F + entity.getWidth();
        }
    }

}
