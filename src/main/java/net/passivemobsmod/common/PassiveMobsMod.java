/*
 * PassiveMobsMod.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.common;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class PassiveMobsMod implements ModInitializer {
    public static final String MODID = "passivemobsmod";

    public static final String BLACKBEAR_NAME = "blackbear";
    public static final String BLACKBEAR_SPAWN_EGG = "blackbear_spawn_egg";

    public static final String BROWNBEAR_NAME = "brownbear";
    public static final String BROWNBEAR_SPAWN_EGG = "brownbear_spawn_egg";

    public static final String COUGAR_NAME = "cougar";
    public static final String COUGAR_SPAWN_EGG = "cougar_spawn_egg";
    public static final String COUGAR_ANGRY_SOUND_NAME = "cougar.angry";
    public static final String COUGAR_AMBIENT_SOUND_NAME = "cougar.ambient";
    public static final String COUGAR_WARNING_SOUND_NAME = "cougar.warning";

    public static final String COYOTE_NAME = "coyote";
    public static final String COYOTE_SPAWN_EGG_NAME = "coyote_spawn_egg";
    public static final String COYOTE_HOWL_NAME = "coyote.howl";

    public static final String CROC_NAME = "croc";
    public static final String CROC_GROWL_NAME = "croc.growl";
    public static final String CROC_HISS_NAME = "croc.hiss";
    public static final String CROC_SPAWN_EGG = "croc_spawn_egg";

    public static final String KOMODO_NAME = "komodo";
    public static final String CHAMELEON_NAME = "chameleon";
    public static final String GRISEUS_NAME = "griseus";
    public static final String KOMODO_SPAWN_EGG_NAME = "komodo_spawn_egg";
    public static final String CHAMELEON_SPAWN_EGG_NAME = "chameleon_spawn_egg";
    public static final String GRISEUS_SPAWN_EGG = "griseus_spawn_egg";

    public static final String HISS_SOUND_NAME = "varanus.hiss";
    public static final String HURT_SOUND_NAME = "varanus.hurt";

    public static final String TARANTULA_NAME = "tarantula";
    public static final String TARANTULA_SPAWN_EGG_NAME = "tarantula_spawn_egg";

    public static final String JUMPER_NAME = "jumper";
    public static final String JUMPER_SPAWN_EGG_NAME = "jumper_spawn_egg";

    public static final String WIDOW_NAME = "widow";
    public static final String WIDOW_SPAWN_EGG_NAME = "widow_spawn_egg";

    public static final String HUNTSMAN_NAME = "huntsman";
    public static final String HUNTSMAN_SPAWN_EGG_NAME = "huntsman_spawn_egg";

    public static final String GREEN_LYNX_NAME = "green_lynx";
    public static final String GREEN_LYNX_SPAWN_EGG_NAME = "green_lynx_spawn_egg";

    public static final String TORTOISE_NAME = "tortoise";
    public static final String TORTOISE_SPAWN_EGG = "tortoise_spawn_egg";

    public static final Identifier CROC_GROWL_SOUND_ID = new Identifier(PassiveMobsMod.MODID, CROC_GROWL_NAME);
    public static final SoundEvent CROC_GROWL_SOUND = new SoundEvent(CROC_GROWL_SOUND_ID);

    public static final Identifier CROC_HISS_SOUND_ID = new Identifier(PassiveMobsMod.MODID, CROC_HISS_NAME);
    public static final SoundEvent CROC_HISS_SOUND = new SoundEvent(CROC_HISS_SOUND_ID);

    public static final Identifier COUGAR_ANGRY_SOUND_ID = new Identifier(PassiveMobsMod.MODID, COUGAR_ANGRY_SOUND_NAME);
    public static final SoundEvent COUGAR_ANGRY_SOUND = new SoundEvent(COUGAR_ANGRY_SOUND_ID);

    public static final Identifier COUGAR_AMBIENT_SOUND_ID = new Identifier(PassiveMobsMod.MODID, COUGAR_AMBIENT_SOUND_NAME);
    public static final SoundEvent COUGAR_AMBIENT_SOUND = new SoundEvent(COUGAR_AMBIENT_SOUND_ID);

    public static final Identifier COUGAR_WARNING_SOUND_ID = new Identifier(PassiveMobsMod.MODID, COUGAR_WARNING_SOUND_NAME);
    public static final SoundEvent COUGAR_WARNING_SOUND = new SoundEvent(COUGAR_WARNING_SOUND_ID);

    public static final Identifier COYOTE_HOWL_SOUND_ID = new Identifier(PassiveMobsMod.MODID, COYOTE_HOWL_NAME);
    public static final SoundEvent COYOTE_HOWL = new SoundEvent(COYOTE_HOWL_SOUND_ID);

    public static final Identifier LIZARD_HISS_SOUND_ID = new Identifier(PassiveMobsMod.MODID, HISS_SOUND_NAME);
    public static final SoundEvent LIZARD_HISS = new SoundEvent(LIZARD_HISS_SOUND_ID);

    public static final Identifier LIZARD_HURT_SOUND_ID = new Identifier(PassiveMobsMod.MODID, HURT_SOUND_NAME);
    public static final SoundEvent LIZARD_HURT = new SoundEvent(LIZARD_HURT_SOUND_ID);

    public static final EntityType<TortoiseEntity> TORTOISE = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(PassiveMobsMod.MODID, TORTOISE_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, TortoiseEntity::new).dimensions(EntityDimensions.fixed(1.0f, 2.5f)).trackedUpdateRate(3).trackRangeBlocks(32).build()
    );

    public final static EntityType<KomodoEntity> KOMODO = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(PassiveMobsMod.MODID, PassiveMobsMod.KOMODO_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, KomodoEntity::new).dimensions(EntityDimensions.fixed(1.0f, 0.3f)).trackRangeBlocks(32).trackedUpdateRate(3).build()
    );

    public final static EntityType<ChameleonEntity> CHAMELEON = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(PassiveMobsMod.MODID, PassiveMobsMod.CHAMELEON_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, ChameleonEntity::new).dimensions(EntityDimensions.fixed(1.0f, 0.3f)).trackRangeBlocks(32).trackedUpdateRate(3).build()
    );

    public final static EntityType<GriseusEntity> GRISEUS = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(PassiveMobsMod.MODID, PassiveMobsMod.GRISEUS_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, GriseusEntity::new).dimensions(EntityDimensions.fixed(1.0f, 0.3f)).trackRangeBlocks(32).trackedUpdateRate(3).build()
    );

    public static final EntityType<CrocEntity> CROCODILE = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(PassiveMobsMod.MODID, PassiveMobsMod.CROC_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, CrocEntity::new).dimensions(EntityDimensions.fixed(1.0f, 2.5f)).trackedUpdateRate(3).trackRangeBlocks(32).build()
    );

    public static final EntityType<CoyoteEntity> COYOTE = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(MODID, COYOTE_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, CoyoteEntity::new).dimensions(EntityDimensions.fixed(1.0f, 2.5f)).trackedUpdateRate(3).trackRangeBlocks(32).build()
    );

    public static final EntityType<CougarEntity> COUGAR = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(MODID, COUGAR_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, CougarEntity::new).dimensions(EntityDimensions.fixed(1.0f, 2.5f)).trackedUpdateRate(3).trackRangeBlocks(32).build()
    );

    public static final EntityType<BlackBearEntity> BLACKBEAR = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(MODID, BLACKBEAR_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, BlackBearEntity::new).dimensions(EntityDimensions.fixed(1.0f, 1.4f)).trackedUpdateRate(3).trackRangeBlocks(32).build()
    );

    public static final EntityType<BrownBearEntity> BROWNBEAR = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(MODID, BROWNBEAR_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, BrownBearEntity::new).dimensions(EntityDimensions.fixed(1.0f, 1.4f)).trackedUpdateRate(3).trackRangeBlocks(32).build()
    );

    public static final EntityType<TarantulaEntity> TARANTULA = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(PassiveMobsMod.MODID, TARANTULA_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, TarantulaEntity::new).dimensions(EntityDimensions.fixed(0.25f, 0.25f)).trackRangeBlocks(32).trackedUpdateRate(3).build()
    );

    public static final EntityType<JumperEntity> JUMPER = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(PassiveMobsMod.MODID, JUMPER_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, JumperEntity::new).dimensions(EntityDimensions.fixed(0.25f, 0.25f)).trackRangeBlocks(32).trackedUpdateRate(3).build()
    );

    public static final EntityType<BlackWidowEntity> WIDOW = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(PassiveMobsMod.MODID, WIDOW_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, BlackWidowEntity::new).dimensions(EntityDimensions.fixed(0.25f, 0.25f)).trackRangeBlocks(32).trackedUpdateRate(3).build()
    );

    public static final EntityType<HuntsmanEntity> HUNTSMAN = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(PassiveMobsMod.MODID, HUNTSMAN_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, HuntsmanEntity::new).dimensions(EntityDimensions.fixed(0.25f, 0.25f)).trackRangeBlocks(32).trackedUpdateRate(3).build()
    );

    public static final EntityType<GreenLynxEntity> GREEN_LYNX = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(PassiveMobsMod.MODID, GREEN_LYNX_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, GreenLynxEntity::new).dimensions(EntityDimensions.fixed(0.25f, 0.25f)).trackRangeBlocks(32).trackedUpdateRate(3).build()
    );

    @Override
    public void onInitialize() {
        FabricDefaultAttributeRegistry.register(BLACKBEAR, BlackBearEntity.createBlackBearAttributes());
        Registry.register(Registry.ITEM, new Identifier(MODID, BLACKBEAR_SPAWN_EGG), new SpawnEggItem(BLACKBEAR, 0x424242, 0x000000, new Item.Settings().group(ItemGroup.MISC)));

        FabricDefaultAttributeRegistry.register(COUGAR, CougarEntity.createAttributes());
        Registry.register(Registry.ITEM, new Identifier(MODID, COUGAR_SPAWN_EGG), new SpawnEggItem(COUGAR, 0x646464, 0x292929, new Item.Settings().group(ItemGroup.MISC)));

        FabricDefaultAttributeRegistry.register(BROWNBEAR, BrownBearEntity.createBrownBearAttributes());
        Registry.register(Registry.ITEM, new Identifier(MODID, BROWNBEAR_SPAWN_EGG), new SpawnEggItem(BROWNBEAR, 0x8B4513, 0x8B5A2B, new Item.Settings().group(ItemGroup.MISC)));

        FabricDefaultAttributeRegistry.register(COYOTE, CoyoteEntity.createCoyoteAttributes());
        Registry.register(Registry.ITEM, new Identifier(MODID, COYOTE_SPAWN_EGG_NAME), new SpawnEggItem(COYOTE, 0x646464, 0x292929, new Item.Settings().group(ItemGroup.MISC)));

        FabricDefaultAttributeRegistry.register(CROCODILE, CrocEntity.createCrocodileAttributes());
        Registry.register(Registry.ITEM, new Identifier(MODID, CROC_SPAWN_EGG), new SpawnEggItem(CROCODILE, 0xCAFF70, 0x556B2F, new Item.Settings().group(ItemGroup.MISC)));

        FabricDefaultAttributeRegistry.register(KOMODO, KomodoEntity.createAttributes());
        FabricDefaultAttributeRegistry.register(CHAMELEON, ChameleonEntity.createAttributes());
        FabricDefaultAttributeRegistry.register(GRISEUS, GriseusEntity.createAttributes());

        Registry.register(Registry.ITEM, new Identifier(MODID, KOMODO_SPAWN_EGG_NAME), new SpawnEggItem(KOMODO, 0x006400, 0x98FB98, new Item.Settings().group(ItemGroup.MISC)));
        Registry.register(Registry.ITEM, new Identifier(MODID, CHAMELEON_SPAWN_EGG_NAME), new SpawnEggItem(CHAMELEON, 0xB22222, 0x228B22, new Item.Settings().group(ItemGroup.MISC)));
        Registry.register(Registry.ITEM, new Identifier(MODID, GRISEUS_SPAWN_EGG), new SpawnEggItem(GRISEUS, 0xCD853F, 0xDEB887, new Item.Settings().group(ItemGroup.MISC)));

        FabricDefaultAttributeRegistry.register(TARANTULA, TarantulaEntity.createAttributes());
        FabricDefaultAttributeRegistry.register(JUMPER, JumperEntity.createAttributes());
        FabricDefaultAttributeRegistry.register(WIDOW, BlackWidowEntity.createAttributes());
        FabricDefaultAttributeRegistry.register(HUNTSMAN, HuntsmanEntity.createAttributes());
        FabricDefaultAttributeRegistry.register(GREEN_LYNX, GreenLynxEntity.createAttributes());

        Registry.register(Registry.ITEM, new Identifier(MODID, TARANTULA_SPAWN_EGG_NAME), new SpawnEggItem(TARANTULA, 0x8B2323, 0x0000CD, new Item.Settings().group(ItemGroup.MISC)));
        Registry.register(Registry.ITEM, new Identifier(MODID, JUMPER_SPAWN_EGG_NAME), new SpawnEggItem(JUMPER, 0x483D8B, 0xDC143C, new Item.Settings().group(ItemGroup.MISC)));
        Registry.register(Registry.ITEM, new Identifier(MODID, WIDOW_SPAWN_EGG_NAME), new SpawnEggItem(WIDOW, 0x292929, 0xDC143C, new Item.Settings().group(ItemGroup.MISC)));
        Registry.register(Registry.ITEM, new Identifier(MODID, HUNTSMAN_SPAWN_EGG_NAME), new SpawnEggItem(HUNTSMAN, 0x8B4513, 0xA52A2A, new Item.Settings().group(ItemGroup.MISC)));
        Registry.register(Registry.ITEM, new Identifier(MODID, GREEN_LYNX_SPAWN_EGG_NAME), new SpawnEggItem(GREEN_LYNX, 0x32cc22, 0x29ff29, new Item.Settings().group(ItemGroup.MISC)));

        FabricDefaultAttributeRegistry.register(TORTOISE, TortoiseEntity.createAttributes());
        Registry.register(Registry.ITEM, new Identifier(MODID, TORTOISE_SPAWN_EGG), new SpawnEggItem(TORTOISE, 0x8B4513, 0x8B4C39, new Item.Settings().group(ItemGroup.MISC)));
    }
}
