/*
 * CoyoteEntityModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import com.google.common.collect.ImmutableList;
import net.passivemobsmod.common.CoyoteEntity;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.entity.model.AnimalModel;
import net.minecraft.util.math.MathHelper;

@Environment(EnvType.CLIENT)
public class CoyoteEntityModel<T extends CoyoteEntity> extends AnimalModel<T> {
    private final ModelPart head;
    private final ModelPart torso;
    private final ModelPart rightBackLeg;
    private final ModelPart leftBackLeg;
    private final ModelPart rightFrontLeg;
    private final ModelPart leftFrontLeg;
    private final ModelPart tail;
    private final ModelPart neck;

    public CoyoteEntityModel() {
        head = new ModelPart(this, 0, 0);
        head.setPivot(-1.0F, 13.5F, -7.0F);
        ModelPart snout = new ModelPart(this, 0, 0);
        snout.addCuboid(-2.0F, -3.0F, -2.0F, 6.0F, 6.0F, 4.0F, 0.0F);
        head.addChild(snout);
        torso = new ModelPart(this, 18, 14);
        torso.addCuboid(-3.0F, -2.0F, -3.0F, 6.0F, 9.0F, 6.0F, 0.0F);
        torso.setPivot(0.0F, 14.0F, 2.0F);
        neck = new ModelPart(this, 21, 0);
        neck.addCuboid(-3.0F, -3.0F, -3.0F, 8.0F, 6.0F, 7.0F, 0.0F);
        neck.setPivot(-1.0F, 14.0F, 2.0F);
        rightBackLeg = new ModelPart(this, 0, 18);
        rightBackLeg.addCuboid(0.0F, 0.0F, -1.0F, 2.0F, 8.0F, 2.0F, 0.0F);
        rightBackLeg.setPivot(-2.5F, 16.0F, 7.0F);
        leftBackLeg = new ModelPart(this, 0, 18);
        leftBackLeg.addCuboid(0.0F, 0.0F, -1.0F, 2.0F, 8.0F, 2.0F, 0.0F);
        leftBackLeg.setPivot(0.5F, 16.0F, 7.0F);
        rightFrontLeg = new ModelPart(this, 0, 18);
        rightFrontLeg.addCuboid(0.0F, 0.0F, -1.0F, 2.0F, 8.0F, 2.0F, 0.0F);
        rightFrontLeg.setPivot(-2.5F, 16.0F, -4.0F);
        leftFrontLeg = new ModelPart(this, 0, 18);
        leftFrontLeg.addCuboid(0.0F, 0.0F, -1.0F, 2.0F, 8.0F, 2.0F, 0.0F);
        leftFrontLeg.setPivot(0.5F, 16.0F, -4.0F);
        tail = new ModelPart(this, 9, 18);
        tail.setPivot(-1.0F, 12.0F, 8.0F);
        ModelPart tailSegment = new ModelPart(this, 9, 18);
        tailSegment.addCuboid(0.0F, 0.0F, -1.0F, 2.0F, 8.0F, 2.0F, 0.0F);
        tail.addChild(tailSegment);
        snout.setTextureOffset(16, 14).addCuboid(-2.0F, -5.0F, 0.0F, 2.0F, 2.0F, 1.0F, 0.0F);
        snout.setTextureOffset(16, 14).addCuboid(2.0F, -5.0F, 0.0F, 2.0F, 2.0F, 1.0F, 0.0F);
        snout.setTextureOffset(0, 10).addCuboid(-0.5F, 0.0F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F);
    }

    protected Iterable<ModelPart> getHeadParts() {
        return ImmutableList.of(head);
    }

    protected Iterable<ModelPart> getBodyParts() {
        return ImmutableList.of(torso, rightBackLeg, leftBackLeg, rightFrontLeg, leftFrontLeg, tail, neck);
    }

    public void animateModel(T coyoteEntity, float f, float g, float h) {
        if (coyoteEntity.hasAngerTime()) {
            tail.yaw = 0.0F;
        } else {
            tail.yaw = MathHelper.cos(f * 0.6662F) * 1.4F * g;
        }

        torso.setPivot(0.0F, 14.0F, 2.0F);
        torso.pitch = 1.5707964F;
        neck.setPivot(-1.0F, 14.0F, -3.0F);
        neck.pitch = torso.pitch;
        tail.setPivot(-1.0F, 12.0F, 8.0F);
        rightBackLeg.setPivot(-2.5F, 16.0F, 7.0F);
        leftBackLeg.setPivot(0.5F, 16.0F, 7.0F);
        rightFrontLeg.setPivot(-2.5F, 16.0F, -4.0F);
        leftFrontLeg.setPivot(0.5F, 16.0F, -4.0F);
        rightBackLeg.pitch = MathHelper.cos(f * 0.6662F) * 1.4F * g;
        leftBackLeg.pitch = MathHelper.cos(f * 0.6662F + 3.1415927F) * 1.4F * g;
        rightFrontLeg.pitch = MathHelper.cos(f * 0.6662F + 3.1415927F) * 1.4F * g;
        leftFrontLeg.pitch = MathHelper.cos(f * 0.6662F) * 1.4F * g;
    }

    public void setAngles(T coyote, float f, float g, float h, float headYaw, float headPitch) {
        head.pitch = headPitch * 0.017453292F;
        head.yaw = headYaw * 0.017453292F;
        tail.pitch = ((float)Math.PI / 4);
    }
}
