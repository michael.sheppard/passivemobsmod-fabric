/*
 * GreenLynxEyesLayer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;


import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.entity.feature.EyesFeatureRenderer;
import net.minecraft.client.render.entity.feature.FeatureRendererContext;
import net.minecraft.entity.Entity;
import net.minecraft.util.Identifier;
import net.passivemobsmod.common.PassiveMobsMod;

public class GreenLynxEyesLayer<T extends Entity, M extends GreenLynxModel<T>> extends EyesFeatureRenderer<T, M> {
    private static final RenderLayer EYES = RenderLayer.getEyes(new Identifier(PassiveMobsMod.MODID, "textures/entity/green_lynx/green_lynx_eyes.png"));

    public GreenLynxEyesLayer(FeatureRendererContext<T, M> rendererContext) {
        super(rendererContext);
    }

    @Override
    public RenderLayer getEyesTexture() {
        return EYES;
    }
}
