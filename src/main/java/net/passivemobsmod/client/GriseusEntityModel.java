/*
 * GriseusEntityModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import com.google.common.collect.ImmutableList;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.passivemobsmod.common.GriseusEntity;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.entity.model.QuadrupedEntityModel;
import net.minecraft.util.math.MathHelper;


@Environment(EnvType.CLIENT)
public class GriseusEntityModel<T extends GriseusEntity> extends QuadrupedEntityModel<T> {
    private final ModelPart tail;

    public GriseusEntityModel() {
        super(12, 0.0f, false, 120.0F, 0.0F, 9.0F, 6.0F, 120);
        float yPos = 19F;

        torso = new ModelPart(this, 21, 16);
        torso.addCuboid(-3F, -2F, -5F, 6, 4, 10);
        torso.setPivot(0.0F, yPos, 0.0F);

        head = new ModelPart(this, 0, 0);
        head.addCuboid(-2F, -2F, -6F, 4, 4, 6);
        head.setPivot(0F, yPos, -5F);

        frontLeftLeg = new ModelPart(this, 56, 1);
        frontLeftLeg.addCuboid(-1F, 0F, -1F, 2, 5, 2);
        frontLeftLeg.setPivot(4F, yPos, -4F);

        backLeftLeg = new ModelPart(this, 56, 1);
        backLeftLeg.addCuboid(-1F, 0F, -1F, 2, 5, 2);
        backLeftLeg.setPivot(4F, yPos, 4F);

        frontRightLeg = new ModelPart(this, 56, 1);
        frontRightLeg.mirror = true;
        frontRightLeg.addCuboid(-1F, 0F, -1F, 2, 5, 2);
        frontRightLeg.setPivot(-4F, yPos, -4F);

        backRightLeg = new ModelPart(this, 56, 1);
        backRightLeg.mirror = true;
        backRightLeg.addCuboid(-1F, 0F, -1F, 2, 5, 2);
        backRightLeg.setPivot(-4F, yPos, 4F);

        tail = new ModelPart(this, 17, 12);
        tail.addCuboid(-1F, -1F, 0F, 2, 2, 18);
        tail.setPivot(0F, yPos, 4F);
    }


    @Override
    public void setAngles(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float headYaw, float headPitch) {
        head.pitch = headPitch * 0.0174532924F;
        head.yaw = headYaw * 0.0174532924F;

        frontLeftLeg.pitch = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        backLeftLeg.pitch = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        frontRightLeg.pitch = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        backRightLeg.pitch = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

        tail.yaw = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
        tail.pitch = 6.021385919380437f;
    }

    @Override
    public Iterable<ModelPart> getBodyParts() {
        return ImmutableList.of(head, torso, frontLeftLeg, backLeftLeg, frontRightLeg, backRightLeg, tail);
    }
}
