/*
 * BlackBearRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;


import net.passivemobsmod.common.BlackBearEntity;
import net.passivemobsmod.common.PassiveMobsMod;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public class BlackBearRenderer extends MobEntityRenderer<BlackBearEntity, BlackBearModel<BlackBearEntity>> {
    private static final Identifier[] BLACK_BEAR_TEXTURES = {
            new Identifier(PassiveMobsMod.MODID, "textures/entity/bear/blackbear.png"),
            new Identifier(PassiveMobsMod.MODID, "textures/entity/bear/cinnamon_blackbear.png")
    };
    private static final float SCALE_FACTOR = 0.9f;

    public BlackBearRenderer(EntityRenderDispatcher renderDispatcher) {
        super(renderDispatcher, new BlackBearModel<>(), 0.5F);
    }

    public Identifier getTexture(BlackBearEntity entity) {
        return BLACK_BEAR_TEXTURES[entity.getVariant()];
    }

    protected void scale(BlackBearEntity blackBearEntity, MatrixStack matrixStack, float partialTickTime) {
        matrixStack.scale(SCALE_FACTOR, SCALE_FACTOR, SCALE_FACTOR);
        super.scale(blackBearEntity, matrixStack, partialTickTime);
    }
}
