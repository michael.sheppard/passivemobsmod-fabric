/*
 * KomodoEntityModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import com.google.common.collect.ImmutableList;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.passivemobsmod.common.KomodoEntity;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.entity.model.QuadrupedEntityModel;
import net.minecraft.util.math.MathHelper;


@Environment(EnvType.CLIENT)
public class KomodoEntityModel<T extends KomodoEntity> extends QuadrupedEntityModel<T> {
    private final ModelPart tail;

    public KomodoEntityModel() {
        super(12, 0.0f, false, 120.0F, 0.0F, 9.0F, 6.0F, 120);
        float yPos = 16F;

        torso = new ModelPart(this, 12, 8);
        torso.addCuboid(-3F, -3F, -8F, 6, 8, 16);
        torso.setPivot(0.0F, yPos, 0.0F);

        head = new ModelPart(this, 0, 0);
        head.addCuboid(-3F, -3F, -8F, 6, 6, 8);
        head.setPivot(0.0F, yPos, -8F);

        frontLeftLeg = new ModelPart(this, 48, 0);
        frontLeftLeg.addCuboid(-2F, 0.0F, -2F, 4, 8, 4);
        frontLeftLeg.setPivot(5F, yPos, -5F);

        backLeftLeg = new ModelPart(this, 48, 0);
        backLeftLeg.addCuboid(-2F, 0.0F, -2F, 4, 8, 4);
        backLeftLeg.setPivot(5F, yPos, 5F);

        frontRightLeg = new ModelPart(this, 48, 0);
        frontRightLeg.mirror = true;
        frontRightLeg.addCuboid(-2F, 0.0F, -2F, 4, 8, 4);
        frontRightLeg.setPivot(-5F, yPos, -5F);

        backRightLeg = new ModelPart(this, 48, 0);
        backRightLeg.mirror = true;
        backRightLeg.addCuboid(-2F, 0.0F, -2F, 4, 8, 4);
        backRightLeg.setPivot(-5F, yPos, 5F);

        tail = new ModelPart(this, 16, 8);
        tail.addCuboid(-2F, -2F, 0.0F, 4, 4, 20);
        tail.setPivot(0.0F, yPos, 6F);
    }

    @Override
    public void setAngles(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float headYaw, float headPitch) {
        head.pitch = headPitch * 0.0174532924F;
        head.yaw =  headYaw * 0.0174532924F;

        frontLeftLeg.pitch = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        backLeftLeg.pitch = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        frontRightLeg.pitch = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        backRightLeg.pitch = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

        tail.yaw = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
        tail.pitch = 5.934119F;
    }

    @Override
    public Iterable<ModelPart> getBodyParts() {
        return ImmutableList.of(torso, frontLeftLeg, backLeftLeg, frontRightLeg, backRightLeg, tail);
    }

    @Override
    public Iterable<ModelPart> getHeadParts() {
        return ImmutableList.of(head);
    }
}
