/*
 * PassiveMobsModClient.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;
import net.passivemobsmod.common.PassiveMobsMod;

@Environment(EnvType.CLIENT)
public class PassiveMobsModClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.BLACKBEAR, ((entityRenderDispatcher, context) -> new BlackBearRenderer(entityRenderDispatcher)));
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.BROWNBEAR, ((entityRenderDispatcher, context) -> new BrownBearRenderer(entityRenderDispatcher)));
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.COUGAR, (((entityRenderDispatcher, context) -> new CougarEntityRenderer(entityRenderDispatcher))));
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.COYOTE, (((entityRenderDispatcher, context) -> new CoyoteEntityRenderer(entityRenderDispatcher))));
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.CROCODILE, (((entityRenderDispatcher, context) -> new CrocRenderer(entityRenderDispatcher))));
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.KOMODO, (((entityRenderDispatcher, context) -> new KomodoEntityRenderer(entityRenderDispatcher))));
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.CHAMELEON, (((entityRenderDispatcher, context) -> new ChameleonEntityRenderer(entityRenderDispatcher))));
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.GRISEUS, (((entityRenderDispatcher, context) -> new GriseusEntityRenderer(entityRenderDispatcher))));
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.TARANTULA, ((entityRenderDispatcher, context) -> new TarantulaRenderer<>(entityRenderDispatcher)));
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.WIDOW, ((entityRenderDispatcher, context) -> new BlackWidowRenderer<>(entityRenderDispatcher)));
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.GREEN_LYNX, ((entityRenderDispatcher, context) -> new GreenLynxRenderer<>(entityRenderDispatcher)));
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.HUNTSMAN, ((entityRenderDispatcher, context) -> new HuntsmanRenderer<>(entityRenderDispatcher)));
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.JUMPER, ((entityRenderDispatcher, context) -> new JumperRenderer<>(entityRenderDispatcher)));
        EntityRendererRegistry.INSTANCE.register(PassiveMobsMod.TORTOISE, (((entityRenderDispatcher, context) -> new TortoiseEntityRenderer(entityRenderDispatcher))));
    }
}
