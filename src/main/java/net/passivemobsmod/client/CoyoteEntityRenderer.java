/*
 * CoyoteEntityRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import net.passivemobsmod.common.CoyoteEntity;
import net.passivemobsmod.common.PassiveMobsMod;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public class CoyoteEntityRenderer extends MobEntityRenderer<CoyoteEntity, CoyoteEntityModel<CoyoteEntity>> {
    private static final Identifier TEXTURE = new Identifier(PassiveMobsMod.MODID, "textures/entity/coyote/coyote.png");
    private static final float X_SCALE = 0.9f;
    private static final float Y_SCALE = 1.1f;
    private static final float Z_SCALE = 1.0f;

    public CoyoteEntityRenderer(EntityRenderDispatcher entityRenderDispatcher) {
        super(entityRenderDispatcher, new CoyoteEntityModel<>(), 0.5F);
    }

    public void render(CoyoteEntity coyote, float f, float g, MatrixStack matrixStack, VertexConsumerProvider vertexConsumerProvider, int i) {
        super.render(coyote, f, g, matrixStack, vertexConsumerProvider, i);
    }

    protected void scale(CoyoteEntity coyoteEntity, MatrixStack matrixStack, float partialTick) {
        matrixStack.scale(X_SCALE, Y_SCALE, Z_SCALE);
        super.scale(coyoteEntity, matrixStack, partialTick);
    }

    public Identifier getTexture(CoyoteEntity coyoteEntity) {
            return TEXTURE;
    }
}
