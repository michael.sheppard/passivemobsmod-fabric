/*
 * CougarEntityModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import com.google.common.collect.ImmutableList;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.entity.model.AnimalModel;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

@Environment(EnvType.CLIENT)
public class CougarEntityModel<T extends Entity> extends AnimalModel<T> {
    protected final ModelPart leftBackLeg;
    protected final ModelPart rightBackLeg;
    protected final ModelPart leftFrontLeg;
    protected final ModelPart rightFrontLeg;
    protected final ModelPart upperTail;
    protected final ModelPart lowerTail;
    protected final ModelPart head = new ModelPart(this);
    protected final ModelPart torso;
    protected int animationState = 1;

    public CougarEntityModel(float scale) {
        super(true, 10.0F, 4.0F);
        head.addCuboid("main", -2.5F, -2.0F, -3.0F, 5, 4, 5, scale, 0, 0);
        head.addCuboid("nose", -1.5F, 0.0F, -4.0F, 3, 2, 2, scale, 0, 24);
        head.addCuboid("ear1", -2.0F, -3.0F, 0.0F, 1, 1, 2, scale, 0, 10);
        head.addCuboid("ear2", 1.0F, -3.0F, 0.0F, 1, 1, 2, scale, 6, 10);
        head.setPivot(0.0F, 15.0F, -9.0F);
        torso = new ModelPart(this, 20, 0);
        torso.addCuboid(-2.0F, 3.0F, -8.0F, 4.0F, 16.0F, 6.0F, scale);
        torso.setPivot(0.0F, 12.0F, -10.0F);
        upperTail = new ModelPart(this, 0, 15);
        upperTail.addCuboid(-0.5F, 0.0F, 0.0F, 1.0F, 8.0F, 1.0F, scale);
        upperTail.pitch = 0.9F;
        upperTail.setPivot(0.0F, 15.0F, 8.0F);
        lowerTail = new ModelPart(this, 4, 15);
        lowerTail.addCuboid(-0.5F, 0.0F, 0.0F, 1.0F, 8.0F, 1.0F, scale);
        lowerTail.setPivot(0.0F, 20.0F, 14.0F);
        leftBackLeg = new ModelPart(this, 8, 13);
        leftBackLeg.addCuboid(-1.0F, 0.0F, 1.0F, 2.0F, 6.0F, 2.0F, scale);
        leftBackLeg.setPivot(1.1F, 18.0F, 5.0F);
        rightBackLeg = new ModelPart(this, 8, 13);
        rightBackLeg.addCuboid(-1.0F, 0.0F, 1.0F, 2.0F, 6.0F, 2.0F, scale);
        rightBackLeg.setPivot(-1.1F, 18.0F, 5.0F);
        leftFrontLeg = new ModelPart(this, 40, 0);
        leftFrontLeg.addCuboid(-1.0F, 0.0F, 0.0F, 2.0F, 10.0F, 2.0F, scale);
        leftFrontLeg.setPivot(1.2F, 14.1F, -5.0F);
        rightFrontLeg = new ModelPart(this, 40, 0);
        rightFrontLeg.addCuboid(-1.0F, 0.0F, 0.0F, 2.0F, 10.0F, 2.0F, scale);
        rightFrontLeg.setPivot(-1.2F, 14.1F, -5.0F);
    }

    protected Iterable<ModelPart> getHeadParts() {
        return ImmutableList.of(head);
    }

    protected Iterable<ModelPart> getBodyParts() {
        return ImmutableList.of(torso, leftBackLeg, rightBackLeg, leftFrontLeg, rightFrontLeg, upperTail, lowerTail);
    }

    public void setAngles(T entity, float limbAngle, float limbDistance, float animationProgress, float headYaw, float headPitch) {
        head.pitch = headPitch * 0.017453292F;
        head.yaw = headYaw * 0.017453292F;
        if (animationState != 3) {
            torso.pitch = 1.5707964F;
            if (animationState == 2) {
                leftBackLeg.pitch = MathHelper.cos(limbAngle * 0.6662F) * limbDistance;
                rightBackLeg.pitch = MathHelper.cos(limbAngle * 0.6662F + 0.3F) * limbDistance;
                leftFrontLeg.pitch = MathHelper.cos(limbAngle * 0.6662F + 3.1415927F + 0.3F) * limbDistance;
                rightFrontLeg.pitch = MathHelper.cos(limbAngle * 0.6662F + 3.1415927F) * limbDistance;
                lowerTail.pitch = 1.7278761F + 0.31415927F * MathHelper.cos(limbAngle) * limbDistance;
            } else {
                leftBackLeg.pitch = MathHelper.cos(limbAngle * 0.6662F) * limbDistance;
                rightBackLeg.pitch = MathHelper.cos(limbAngle * 0.6662F + 3.1415927F) * limbDistance;
                leftFrontLeg.pitch = MathHelper.cos(limbAngle * 0.6662F + 3.1415927F) * limbDistance;
                rightFrontLeg.pitch = MathHelper.cos(limbAngle * 0.6662F) * limbDistance;
                if (animationState == 1) {
                    lowerTail.pitch = 1.7278761F + 0.7853982F * MathHelper.cos(limbAngle) * limbDistance;
                } else {
                    lowerTail.pitch = 1.7278761F + 0.47123894F * MathHelper.cos(limbAngle) * limbDistance;
                }
            }
        }

    }

    public void animateModel(T entity, float limbAngle, float limbDistance, float tickDelta) {
        torso.pivotY = 12.0F;
        torso.pivotZ = -10.0F;
        head.pivotY = 15.0F;
        head.pivotZ = -9.0F;
        upperTail.pivotY = 15.0F;
        upperTail.pivotZ = 8.0F;
        lowerTail.pivotY = 20.0F;
        lowerTail.pivotZ = 14.0F;
        leftFrontLeg.pivotY = 14.1F;
        leftFrontLeg.pivotZ = -5.0F;
        rightFrontLeg.pivotY = 14.1F;
        rightFrontLeg.pivotZ = -5.0F;
        leftBackLeg.pivotY = 18.0F;
        leftBackLeg.pivotZ = 5.0F;
        rightBackLeg.pivotY = 18.0F;
        rightBackLeg.pivotZ = 5.0F;
        upperTail.pitch = 0.9F;
        ModelPart headPart;
        if (entity.isInSneakingPose()) {
            ++torso.pivotY;
            headPart = head;
            headPart.pivotY += 2.0F;
            ++upperTail.pivotY;
            headPart = lowerTail;
            headPart.pivotY += -4.0F;
            headPart = lowerTail;
            headPart.pivotZ += 2.0F;
            upperTail.pitch = 1.5707964F;
            lowerTail.pitch = 1.5707964F;
            animationState = 0;
        } else if (entity.isSprinting()) {
            lowerTail.pivotY = upperTail.pivotY;
            headPart = lowerTail;
            headPart.pivotZ += 2.0F;
            upperTail.pitch = 1.5707964F;
            lowerTail.pitch = 1.5707964F;
            animationState = 2;
        } else {
            animationState = 1;
        }

    }
}
