/*
 * BlackbearSpawnMixin.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.mixin;

import net.passivemobsmod.common.PassiveMobsMod;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.world.biome.SpawnSettings;
import net.minecraft.world.gen.feature.DefaultBiomeFeatures;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(DefaultBiomeFeatures.class)
public abstract class PassiveMobsSpawnMixin {
    @Inject(at = @At("TAIL"), method = "addFarmAnimals")
    private static void AddTemperateSpawn(SpawnSettings.Builder builder, CallbackInfo info) {
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.BLACKBEAR, 12, 1, 2));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry( PassiveMobsMod.BROWNBEAR, 12, 1, 2));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.COUGAR, 12, 1, 2));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.COYOTE, 10, 2, 6));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.CROCODILE, 50, 2, 4));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.KOMODO, 12, 1, 4));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.CHAMELEON, 12, 1, 4));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.JUMPER, 12, 1, 4));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.HUNTSMAN, 12, 1, 4));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.GREEN_LYNX, 12, 1, 4));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.WIDOW, 12, 1, 4));
    }

    @Inject(at = @At("TAIL"), method = "addSnowyMobs")
    private static void AddSnowySpawn(SpawnSettings.Builder builder, CallbackInfo info) {
        builder.spawn( SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.BLACKBEAR, 10, 1, 2));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.BROWNBEAR, 10, 1, 2));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.COYOTE, 10, 2, 6));
    }

    @Inject(at = @At("TAIL"), method = "addJungleMobs")
    private static void addCougarJungleSpawn(SpawnSettings.Builder builder, CallbackInfo info) {
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.COUGAR, 12, 1, 2));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.CHAMELEON, 12, 1, 4));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.WIDOW, 12, 1, 4));
    }

    @Inject(at = @At("HEAD"), method = "addPlainsMobs")
    private static void AddCoyotePlainsSpawn(SpawnSettings.Builder builder, CallbackInfo info) {
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.COYOTE, 10, 2, 6));
    }

    @Inject(at = @At("HEAD"), method = "addDesertMobs")
    private static void addDesertSpawn(SpawnSettings.Builder builder, CallbackInfo info) {
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.GRISEUS, 12, 1, 4));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.TARANTULA, 12, 1, 4));
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.TORTOISE, 12, 1, 4));
    }
}
