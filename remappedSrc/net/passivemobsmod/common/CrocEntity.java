/*
 * CrocEntity.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.common;

import net.minecraft.block.BlockState;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeKeys;

import java.util.Objects;
import java.util.Optional;
import java.util.Random;


public class CrocEntity extends AnimalEntity {
    private final float WIDTH = 1.0f;
    private final float HEIGHT = 0.3f;
    private final float scaleFactor;
    public EntityDimensions crocSize = new EntityDimensions(1.0f, 0.3f, false);

    public CrocEntity(EntityType<? extends CrocEntity> entity, World world) {
        super(entity, world);

        float scale = random.nextFloat();
        scaleFactor = scale < 0.6F ? 1.0F : scale;
        crocSize.scaled(WIDTH * getScaleFactor(), HEIGHT * getScaleFactor());
    }

    @Override
    protected void initGoals() {
        goalSelector.add(0, new SwimGoal(this));
        goalSelector.add(1, new PounceAtTargetGoal(this, 0.5F));
        goalSelector.add(2, new MeleeAttackGoal(this, 1.0, true));
        goalSelector.add(3, new WanderAroundGoal(this, 1.0));
        goalSelector.add(4, new LookAtEntityGoal(this, PlayerEntity.class, 8.0F));
        goalSelector.add(5, new LookAroundGoal(this));

        targetSelector.add(1, new RevengeGoal(this));
        targetSelector.add(2, new CrocNearestAttackableTargetGoal<>(this, PlayerEntity.class, true, true));
        targetSelector.add(3, new CrocNearestAttackableTargetGoal<>(this, SheepEntity.class, false, true));
        targetSelector.add(3, new CrocNearestAttackableTargetGoal<>(this, PigEntity.class, false, true));
        targetSelector.add(3, new CrocNearestAttackableTargetGoal<>(this, RabbitEntity.class, false, true));
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    @Override
    public EntityType<?> getType() {
        return PassiveMobsMod.CROCODILE;
    }

    // implementing Biome specific spawning here
    public static boolean canSpawn(EntityType<CrocEntity> type, WorldAccess world, SpawnReason spawnReason, BlockPos pos, Random random) {
        return validSpawnBiomes(world, pos);
    }

    private static boolean validSpawnBiomes(WorldAccess world, BlockPos pos) {
        Optional<RegistryKey<Biome>> optional = world.method_31081(pos);
        return Objects.equals(optional, Optional.of(BiomeKeys.SWAMP)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.SWAMP_HILLS)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.RIVER)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.BEACH));
    }

    protected float getActiveEyeHeight(EntityPose pose, EntityDimensions dimensions) {
        return dimensions.height * 0.2F;
    }

    @Override
    public EntityDimensions getDimensions(EntityPose pose) {
        return new EntityDimensions(WIDTH, HEIGHT, false);
    }

    @Override
    public void tick() {
        // kill in cold biomes
        Vec3d v = getPos();
        BlockPos bp = new BlockPos(v.x, v.y, v.z);
        Biome biome = world.getBiome(bp);
        if (biome.getTemperature(bp) <= 0.25) {
            damage(DamageSource.STARVE, 4.0f);
        }
        super.tick();
    }

    @Override
    public boolean cannotDespawn() {
        return true;
    }

    public static DefaultAttributeContainer.Builder createCrocodileAttributes() {
        return MobEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MAX_HEALTH, 10.0D)
                .add(EntityAttributes.GENERIC_FOLLOW_RANGE, 20.0D)
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.25D)
                .add(EntityAttributes.GENERIC_ATTACK_DAMAGE, 3.0D);
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return PassiveMobsMod.CROC_GROWL_SOUND;
    }

    @Override
    protected SoundEvent getDeathSound() {
        return PassiveMobsMod.CROC_GROWL_SOUND;
    }

    @Override
    protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
        return PassiveMobsMod.CROC_HISS_SOUND;
    }

    @Override
    protected void playStepSound(BlockPos pos, BlockState blockIn) {
        playSound(getStepSound(), 0.15F, 1.0F);
    }

    protected SoundEvent getStepSound() {
        return SoundEvents.ENTITY_COW_STEP;
    }

    @Override
    protected float getSoundVolume() {
        return 0.4F;
    }

    @Override
    public boolean tryAttack(Entity target) {
        boolean damage = target.damage(DamageSource.mob(this), (float) ((int) getAttributeValue(EntityAttributes.GENERIC_ATTACK_DAMAGE)));
        if (damage) {
            dealDamage(this, target);
        }

        return damage;
    }

    @Override
    public PassiveEntity createChild(ServerWorld serverWorld, PassiveEntity passiveEntity) {
        return PassiveMobsMod.CROCODILE.create(serverWorld);
    }
}
