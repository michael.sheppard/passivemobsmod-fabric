/*
 * KomodoEntity.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.common;

import net.minecraft.block.BlockState;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.passive.PigEntity;
import net.minecraft.entity.passive.RabbitEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeKeys;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Optional;
import java.util.Random;

public class KomodoEntity extends AnimalEntity {
    private final float WIDTH = 1.0f;
    private final float HEIGHT = 0.75f;
    private final float scaleFactor;
    public EntityDimensions lizardSize = new EntityDimensions(1.0f, 0.3f, false);

    public KomodoEntity(EntityType<? extends KomodoEntity> entityType, World world) {
        super(entityType, world);

        float scale = random.nextFloat();
        scaleFactor = scale < 0.55F ? 1.0F : scale;
        lizardSize.scaled(WIDTH * scaleFactor, HEIGHT * scaleFactor);
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    @Override
    public EntityDimensions getDimensions(EntityPose pose) {
        return new EntityDimensions(WIDTH * scaleFactor, HEIGHT * scaleFactor, false);
    }

    protected float getActiveEyeHeight(EntityPose pose, EntityDimensions dimensions) {
        return dimensions.height * 0.9f;
    }

    public static DefaultAttributeContainer.Builder createAttributes() {
        return MobEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MAX_HEALTH, 10.0D)
                .add(EntityAttributes.GENERIC_FOLLOW_RANGE, 10.0D)
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.2D);
    }

    protected void initGoals() {
        goalSelector.add(0, new SwimGoal(this));
        goalSelector.add(1, new EscapeDangerGoal(this, 2.0D));
        goalSelector.add(4, new FollowParentGoal(this, 1.25D));
        goalSelector.add(5, new WanderAroundFarGoal(this, 1.0D));
        goalSelector.add(6, new LookAtEntityGoal(this, PlayerEntity.class, 6.0F));
        goalSelector.add(7, new LookAroundGoal(this));
        targetSelector.add(1, new FollowTargetGoal<>(this, AnimalEntity.class, 10, false, false,
                (livingEntity) -> livingEntity instanceof PigEntity || livingEntity instanceof RabbitEntity));
    }

    // implementing Biome specific spawning here
    @SuppressWarnings("unused")
    public static boolean canSpawn(EntityType<KomodoEntity> type, WorldAccess world, SpawnReason spawnReason, BlockPos pos, Random random) {
        return validSpawnBiomes(world, pos);
    }

    private static boolean validSpawnBiomes(WorldAccess world, BlockPos pos) {
        Optional<RegistryKey<Biome>> optional = world.method_31081(pos);
        return Objects.equals(optional, Optional.of(BiomeKeys.SAVANNA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.SAVANNA_PLATEAU)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.SHATTERED_SAVANNA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.SHATTERED_SAVANNA_PLATEAU)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.PLAINS));
    }

    protected SoundEvent getAmbientSound() {
        return PassiveMobsMod.LIZARD_HISS;
    }

    protected SoundEvent getHurtSound(DamageSource source) {
        return PassiveMobsMod.LIZARD_HURT;
    }

    protected SoundEvent getDeathSound() {
        return PassiveMobsMod.LIZARD_HURT;
    }

    protected void playStepSound(BlockPos pos, BlockState state) {
        playSound(SoundEvents.ENTITY_PIG_STEP, 0.15F, 1.0F);
    }

    protected float getSoundVolume() {
        return 0.4F;
    }

    @Override
    public @Nullable PassiveEntity createChild(ServerWorld world, PassiveEntity entity) {
        return PassiveMobsMod.KOMODO.create(world);
    }
}
