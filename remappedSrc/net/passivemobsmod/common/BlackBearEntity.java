/*
 * BlackEntity.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.common;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.Durations;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.mob.Angerable;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.FoxEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.IntRange;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.LocalDifficulty;
import net.minecraft.world.ServerWorldAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeKeys;

import java.util.*;

public class BlackBearEntity extends AnimalEntity implements Angerable {
    private static final TrackedData<Boolean> WARNING;
    private float lastWarningAnimationProgress;
    private float warningAnimationProgress;
    private int warningSoundCooldown;
    private static final IntRange RANGE;
    private int angerTime;
    private UUID targetUuid;

    private static final TrackedData<Integer> VARIANT;

    public BlackBearEntity(EntityType<? extends BlackBearEntity> entity, World world) {
        super(entity, world);
    }

    public PassiveEntity createChild(ServerWorld serverWorld, PassiveEntity passiveEntity) {
        return PassiveMobsMod.BLACKBEAR.create(serverWorld);
    }

    public boolean isBreedingItem(ItemStack stack) {
        return false;
    }

    public boolean cannotDespawn() { return true; }

    protected void initGoals() {
        super.initGoals();
        goalSelector.add(0, new SwimGoal(this));
        goalSelector.add(1, new BlackBearEntity.AttackGoal());
        goalSelector.add(1, new BlackBearEntity.PolarBearEscapeDangerGoal());
        goalSelector.add(4, new FollowParentGoal(this, 1.25D));
        goalSelector.add(5, new WanderAroundGoal(this, 1.0D));
        goalSelector.add(6, new LookAtEntityGoal(this, PlayerEntity.class, 6.0F));
        goalSelector.add(7, new LookAroundGoal(this));
        targetSelector.add(1, new BlackBearEntity.PolarBearRevengeGoal());
        targetSelector.add(2, new BlackBearEntity.FollowPlayersGoal());
        targetSelector.add(3, new FollowTargetGoal<>(this, PlayerEntity.class, 10, true, false, this::shouldAngerAt));
        targetSelector.add(4, new FollowTargetGoal<>(this, FoxEntity.class, 10, true, true, null));
        targetSelector.add(5, new UniversalAngerGoal<>(this, false));
    }

    public static DefaultAttributeContainer.Builder createBlackBearAttributes() {
        return MobEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MAX_HEALTH, 30.0D)
                .add(EntityAttributes.GENERIC_FOLLOW_RANGE, 20.0D)
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.25D)
                .add(EntityAttributes.GENERIC_ATTACK_DAMAGE, 6.0D);
    }

    // implementing Biome specific spawning here
    @SuppressWarnings("unused")
    public static boolean canSpawn(EntityType<BlackBearEntity> type, WorldAccess world, SpawnReason spawnReason, BlockPos pos, Random random) {
        return validSpawnBiomes(world, pos);
    }

    private static boolean validSpawnBiomes(WorldAccess world, BlockPos pos) {
        Optional<RegistryKey<Biome>> optional = world.method_31081(pos);
        return Objects.equals(optional, Optional.of(BiomeKeys.PLAINS)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.BIRCH_FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.FLOWER_FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.MOUNTAINS)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.TAIGA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.GIANT_SPRUCE_TAIGA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.SNOWY_TAIGA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.GIANT_TREE_TAIGA));
    }

    public void readCustomDataFromTag(CompoundTag tag) {
        super.readCustomDataFromTag(tag);
        setVariant(tag.getInt("Variant"));
        angerFromTag((ServerWorld) world, tag);
    }

    public void writeCustomDataToTag(CompoundTag tag) {
        super.writeCustomDataToTag(tag);
        tag.putInt("Variant", getVariant());
        angerToTag(tag);
    }

    public void chooseRandomAngerTime() {
        setAngerTime(RANGE.choose(random));
    }

    public void setAngerTime(int ticks) {
        angerTime = ticks;
    }

    public int getAngerTime() {
        return angerTime;
    }

    public void setAngryAt(UUID uuid) {
        targetUuid = uuid;
    }

    public UUID getAngryAt() {
        return targetUuid;
    }

    protected SoundEvent getAmbientSound() {
        return isBaby() ? SoundEvents.ENTITY_POLAR_BEAR_AMBIENT_BABY : SoundEvents.ENTITY_POLAR_BEAR_AMBIENT;
    }

    protected SoundEvent getHurtSound(DamageSource source) {
        return SoundEvents.ENTITY_POLAR_BEAR_HURT;
    }

    protected SoundEvent getDeathSound() {
        return SoundEvents.ENTITY_POLAR_BEAR_DEATH;
    }

    protected void playStepSound(BlockPos pos, BlockState state) {
        playSound(SoundEvents.ENTITY_POLAR_BEAR_STEP, 0.15F, 1.0F);
    }

    protected void playWarningSound() {
        if (warningSoundCooldown <= 0) {
            playSound(SoundEvents.ENTITY_POLAR_BEAR_WARNING, 1.0F, getSoundPitch());
            warningSoundCooldown = 40;
        }

    }

    protected void initDataTracker() {
        super.initDataTracker();
        dataTracker.startTracking(WARNING, false);
        dataTracker.startTracking(VARIANT, 0);
    }

    public void tick() {
        super.tick();
        if (world.isClient) {
            if (warningAnimationProgress != lastWarningAnimationProgress) {
                calculateDimensions();
            }

            lastWarningAnimationProgress = warningAnimationProgress;
            if (isWarning()) {
                warningAnimationProgress = MathHelper.clamp(warningAnimationProgress + 1.0F, 0.0F, 6.0F);
            } else {
                warningAnimationProgress = MathHelper.clamp(warningAnimationProgress - 1.0F, 0.0F, 6.0F);
            }
        }

        if (warningSoundCooldown > 0) {
            --warningSoundCooldown;
        }

        if (!world.isClient) {
            tickAngerLogic((ServerWorld) world, true);
        }

    }

    public EntityDimensions getDimensions(EntityPose pose) {
        if (warningAnimationProgress > 0.0F) {
            float animationProgress = warningAnimationProgress / 6.0F;
            float onePlus = 1.0F + animationProgress;
            return super.getDimensions(pose).scaled(1.0F, onePlus);
        } else {
            return super.getDimensions(pose);
        }
    }

    public boolean tryAttack(Entity target) {
        boolean damage = target.damage(DamageSource.mob(this), (float) ((int) getAttributeValue(EntityAttributes.GENERIC_ATTACK_DAMAGE)));
        if (damage) {
            dealDamage(this, target);
        }

        return damage;
    }

    public boolean isWarning() {
        return dataTracker.get(WARNING);
    }

    public void setWarning(boolean warning) {
        dataTracker.set(WARNING, warning);
    }

    public int getVariant() {
        return MathHelper.clamp(dataTracker.get(VARIANT), 0, 3);
    }

    public void setVariant(int variantIn) {
        dataTracker.set(VARIANT, variantIn);
    }

    @Environment(EnvType.CLIENT)
    public float getWarningAnimationProgress(float tickDelta) {
        return MathHelper.lerp(tickDelta, lastWarningAnimationProgress, warningAnimationProgress) / 6.0F;
    }

    protected float getBaseMovementSpeedMultiplier() {
        return 0.98F;
    }

    public EntityData initialize(ServerWorldAccess serverWorldAccess, LocalDifficulty difficulty, SpawnReason spawnReason, EntityData entityData, CompoundTag entityTag) {
        if (random.nextInt(8) == 0) {
            setVariant(1); // cinnamon phase is rare
        } else {
            setVariant(0);
        }
        if (entityData == null) {
            entityData = new PassiveEntity.PassiveData(1.0F);
        }

        return super.initialize(serverWorldAccess, difficulty, spawnReason, entityData, entityTag);
    }

    static {
        WARNING = DataTracker.registerData(BlackBearEntity.class, TrackedDataHandlerRegistry.BOOLEAN);
        VARIANT = DataTracker.registerData(BlackBearEntity.class, TrackedDataHandlerRegistry.INTEGER);
        RANGE = Durations.betweenSeconds(20, 39);
    }

    class PolarBearEscapeDangerGoal extends EscapeDangerGoal {
        public PolarBearEscapeDangerGoal() {
            super(BlackBearEntity.this, 2.0D);
        }

        public boolean canStart() {
            return (BlackBearEntity.this.isBaby() || BlackBearEntity.this.isOnFire()) && super.canStart();
        }
    }

    class AttackGoal extends MeleeAttackGoal {
        public AttackGoal() {
            super(BlackBearEntity.this, 1.25D, true);
        }

        protected void attack(LivingEntity target, double squaredDistance) {
            double attackDistance = getSquaredMaxAttackDistance(target);
            if (squaredDistance <= attackDistance && method_28347()) {
                method_28346();
                mob.tryAttack(target);
                BlackBearEntity.this.setWarning(false);
            } else if (squaredDistance <= attackDistance * 2.0D) {
                if (method_28347()) {
                    BlackBearEntity.this.setWarning(false);
                    method_28346();
                }

                if (method_28348() <= 10) {
                    BlackBearEntity.this.setWarning(true);
                    BlackBearEntity.this.playWarningSound();
                }
            } else {
                method_28346();
                BlackBearEntity.this.setWarning(false);
            }

        }

        public void stop() {
            BlackBearEntity.this.setWarning(false);
            super.stop();
        }

        protected double getSquaredMaxAttackDistance(LivingEntity entity) {
            return 4.0F + entity.getWidth();
        }
    }

    class FollowPlayersGoal extends FollowTargetGoal<PlayerEntity> {
        public FollowPlayersGoal() {
            super(BlackBearEntity.this, PlayerEntity.class, 20, true, true, null);
        }

        public boolean canStart() {
            if (!BlackBearEntity.this.isBaby()) {
                if (super.canStart()) {
                    List<BlackBearEntity> list = BlackBearEntity.this.world.getNonSpectatingEntities(BlackBearEntity.class, BlackBearEntity.this.getBoundingBox().expand(8.0D, 4.0D, 8.0D));

                    for (BlackBearEntity BlackBearEntity : list) {
                        if (BlackBearEntity.isBaby()) {
                            return true;
                        }
                    }
                }

            }
            return false;
        }

        protected double getFollowRange() {
            return super.getFollowRange() * 0.5D;
        }
    }

    class PolarBearRevengeGoal extends RevengeGoal {
        public PolarBearRevengeGoal() {
            super(BlackBearEntity.this);
        }

        public void start() {
            super.start();
            if (BlackBearEntity.this.isBaby()) {
                callSameTypeForRevenge();
                stop();
            }

        }

        protected void setMobEntityTarget(MobEntity mob, LivingEntity target) {
            if (mob instanceof BlackBearEntity && !mob.isBaby()) {
                super.setMobEntityTarget(mob, target);
            }

        }
    }

}
