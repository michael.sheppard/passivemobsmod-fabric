/*
 * DefaultBiomeCreatorMixin.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.mixin;

//import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
//import net.minecraft.sound.BiomeMoodSound;
//import net.minecraft.util.math.MathHelper;
import net.minecraft.world.biome.*;
//import net.minecraft.world.gen.GenerationStep;
//import net.minecraft.world.gen.feature.ConfiguredFeatures;
//import net.minecraft.world.gen.feature.ConfiguredStructureFeatures;
//import net.minecraft.world.gen.feature.DefaultBiomeFeatures;
//import net.minecraft.world.gen.surfacebuilder.ConfiguredSurfaceBuilders;
import net.passivemobsmod.common.PassiveMobsMod;
import org.spongepowered.asm.mixin.Mixin;
//import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(DefaultBiomeCreator.class)
public abstract class DefaultBiomeCreatorMixin {

    @Inject(at = @At("TAIL"), method = "createBeach")
    private static void addCrocBeachSpawn(float depth, float scale, float temperature, float downfall, int waterColor, boolean snowy, boolean stony, CallbackInfoReturnable<Biome> info) {
        SpawnSettings.Builder builder = new SpawnSettings.Builder();
        if (!stony && !snowy) {
            builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.CROCODILE, 50, 2, 5));
        }
    }

    @Inject(at = @At("TAIL"), method = "createSwamp")
    private static void addCrocToSwamp(float depth, float scale, boolean bl, CallbackInfoReturnable<Biome> info) {
        SpawnSettings.Builder builder = new SpawnSettings.Builder();
        builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.CROCODILE, 50, 2, 5));
    }

    @Inject(at = @At("HEAD"), method = "createRiver")
    private static void addCrocToRiver(float depth, float scale, float temperature, int waterColor, boolean bl, CallbackInfoReturnable<Biome> info) {
        SpawnSettings.Builder builder = (new SpawnSettings.Builder()).spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.CROCODILE, 50, 2, 5));
        if (temperature > 0.7) {
            builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.CROCODILE, 50, 2, 5));
        }
    }

//    /**
//     * @author crackedEgg
//     *
//     * @reason needed to overwrite createRiver
//     */
//    @Overwrite
//    private static int getSkyColor(float temperature) {
//        float f = temperature / 3.0F;
//        f = MathHelper.clamp(f, -1.0F, 1.0F);
//        return MathHelper.hsvToRgb(0.62222224F - f * 0.05F, 0.5F + f * 0.1F, 1.0F);
//    }

//    /**
//     * @author crackedEgg
//     *
//     * @reason eliminate squid spawns in rivers (it's just annoying as fuck)
//     * @reason add crocodile spawns to rivers
//     */
//    @Overwrite
//    public static Biome createRiver(float depth, float scale, float temperature, int waterColor, boolean bl) {
//        SpawnSettings.Builder builder = (new SpawnSettings.Builder()).spawn(SpawnGroup.WATER_AMBIENT, new SpawnSettings.SpawnEntry(EntityType.SALMON, 5, 1, 5));
//        if (temperature > 0.7) {
//            builder.spawn(SpawnGroup.CREATURE, new SpawnSettings.SpawnEntry(PassiveMobsMod.CROCODILE, 50, 2, 5));
//        }
//        DefaultBiomeFeatures.addBatsAndMonsters(builder);
//        builder.spawn(SpawnGroup.MONSTER, new SpawnSettings.SpawnEntry(EntityType.DROWNED, bl ? 1 : 100, 1, 1));
//        GenerationSettings.Builder builder2 = (new GenerationSettings.Builder()).surfaceBuilder(ConfiguredSurfaceBuilders.GRASS);
//        builder2.structureFeature(ConfiguredStructureFeatures.MINESHAFT);
//        builder2.structureFeature(ConfiguredStructureFeatures.RUINED_PORTAL);
//        DefaultBiomeFeatures.addLandCarvers(builder2);
//        DefaultBiomeFeatures.addDefaultLakes(builder2);
//        DefaultBiomeFeatures.addDungeons(builder2);
//        DefaultBiomeFeatures.addMineables(builder2);
//        DefaultBiomeFeatures.addDefaultOres(builder2);
//        DefaultBiomeFeatures.addDefaultDisks(builder2);
//        DefaultBiomeFeatures.addWaterBiomeOakTrees(builder2);
//        DefaultBiomeFeatures.addDefaultFlowers(builder2);
//        DefaultBiomeFeatures.addDefaultGrass(builder2);
//        DefaultBiomeFeatures.addDefaultMushrooms(builder2);
//        DefaultBiomeFeatures.addDefaultVegetation(builder2);
//        DefaultBiomeFeatures.addSprings(builder2);
//        if (!bl) {
//            builder2.feature(GenerationStep.Feature.VEGETAL_DECORATION, ConfiguredFeatures.SEAGRASS_RIVER);
//        }
//
//        DefaultBiomeFeatures.addFrozenTopLayer(builder2);
//        return (new Biome.Builder()).precipitation(bl ? Biome.Precipitation.SNOW : Biome.Precipitation.RAIN).category(Biome.Category.RIVER).depth(depth).scale(scale).temperature(temperature).downfall(0.5F).effects((new BiomeEffects.Builder()).waterColor(waterColor).waterFogColor(329011).fogColor(12638463).skyColor(getSkyColor(temperature)).moodSound(BiomeMoodSound.CAVE).build()).spawnSettings(builder.build()).generationSettings(builder2.build()).build();
//    }
}
