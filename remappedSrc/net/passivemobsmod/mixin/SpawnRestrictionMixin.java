/*
 * SpawnRestrictionMixin.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.mixin;

import net.passivemobsmod.common.*;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnRestriction;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.world.Heightmap;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(SpawnRestriction.class)
public abstract class SpawnRestrictionMixin {
    @Shadow
    private static <T extends MobEntity> void register(EntityType<T> type, SpawnRestriction.Location location, Heightmap.Type heightmapType, SpawnRestriction.SpawnPredicate<T> predicate){}

    static {
        register(PassiveMobsMod.BLACKBEAR, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, BlackBearEntity::canSpawn);
        register(PassiveMobsMod.BROWNBEAR, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, BrownBearEntity::canSpawn);
        register(PassiveMobsMod.COUGAR, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, CougarEntity::canSpawn);
        register(PassiveMobsMod.COYOTE, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, CoyoteEntity::canSpawn);
        register(PassiveMobsMod.CROCODILE, SpawnRestriction.Location.NO_RESTRICTIONS, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, CrocEntity::canSpawn);
        register(PassiveMobsMod.KOMODO, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, KomodoEntity::canSpawn);
        register(PassiveMobsMod.CHAMELEON, SpawnRestriction.Location.NO_RESTRICTIONS, Heightmap.Type.MOTION_BLOCKING, ChameleonEntity::canSpawn);
        register(PassiveMobsMod.GRISEUS, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, GriseusEntity::canSpawn);
        register(PassiveMobsMod.WIDOW, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING, BlackWidowEntity::canSpawn);
        register(PassiveMobsMod.JUMPER, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING, JumperEntity::canSpawn);
        register(PassiveMobsMod.HUNTSMAN, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING, HuntsmanEntity::canSpawn);
        register(PassiveMobsMod.GREEN_LYNX, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING, GreenLynxEntity::canSpawn);
        register(PassiveMobsMod.TARANTULA, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING, TarantulaEntity::canSpawn);
        register(PassiveMobsMod.TORTOISE, SpawnRestriction.Location.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, TortoiseEntity::canSpawn);
    }
}
