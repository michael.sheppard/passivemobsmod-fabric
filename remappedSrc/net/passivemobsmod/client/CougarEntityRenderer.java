/*
 * CougarEntityRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.passivemobsmod.common.CougarEntity;
import net.passivemobsmod.common.PassiveMobsMod;

@Environment(EnvType.CLIENT)
public class CougarEntityRenderer extends MobEntityRenderer<CougarEntity, CougarEntityModel<CougarEntity>> {
    private static final Identifier TEXTURE = new Identifier(PassiveMobsMod.MODID, "textures/entity/cougar/cougar.png");
    private static final float SCALE_FACTOR = 2.2f;

    public CougarEntityRenderer(EntityRenderDispatcher entityRenderDispatcher) {
        super(entityRenderDispatcher, new CougarEntityModel<>(0.0F), 0.4F);
    }

    public Identifier getTexture(CougarEntity ocelotEntity) {
        return TEXTURE;
    }

    protected void scale(CougarEntity cougarEntity, MatrixStack matrixStack, float partialTick) {
        matrixStack.scale(SCALE_FACTOR, SCALE_FACTOR, SCALE_FACTOR);
        super.scale(cougarEntity, matrixStack, partialTick);
    }
}
