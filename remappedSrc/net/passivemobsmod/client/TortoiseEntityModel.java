/*
 * TortoiseEntityModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import com.google.common.collect.ImmutableList;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.entity.model.QuadrupedEntityModel;
import net.minecraft.util.math.MathHelper;
import net.passivemobsmod.common.TortoiseEntity;

@Environment(EnvType.CLIENT)
public class TortoiseEntityModel<T extends TortoiseEntity> extends QuadrupedEntityModel<T> {
    private final ModelPart tail;
    private final ModelPart plastron;

    public TortoiseEntityModel() {
        super(12, 0.0f, false, 120.0F, 0.0F, 9.0F, 6.0F, 120);
        float yPos = 22F;

        torso = new ModelPart(this, 0, 23);
        torso.addCuboid(-3F, 0F, -3F, 6, 3, 6);
        torso.setPivot(0F, yPos - 4, 0F);

        head = new ModelPart(this, 0, 0);
        head.addCuboid(-2F, 0F, -4F, 4, 3, 4);
        head.setPivot(0F, yPos - 3, -4F);

        frontLeftLeg = new ModelPart(this, 60, 0);
        frontLeftLeg.addCuboid(0F, 0F, 0F, 1, 3, 1);
        frontLeftLeg.setPivot(2F, yPos, -3F);
        frontLeftLeg.roll = 5.497787143782138F;

        backLeftLeg = new ModelPart(this, 60, 0);
        backLeftLeg.addCuboid(0F, 0F, 0F, 1, 3, 1);
        backLeftLeg.setPivot(2F, yPos, 2F);
        backLeftLeg.roll = 5.497787143782138F;

        frontRightLeg = new ModelPart(this, 60, 0);
        frontRightLeg.addCuboid(-1F, 0F, 0F, 1, 3, 1);
        frontRightLeg.setPivot(-2F, yPos, -3F);
        frontRightLeg.roll = 0.7853981633974483F;

        backRightLeg = new ModelPart(this, 60, 0);
        backRightLeg.addCuboid(-1F, 0F, 0F, 1, 3, 1);
        backRightLeg.setPivot(-2F, yPos, 2F);
        backRightLeg.roll = 0.7853981633974483F;

        plastron = new ModelPart(this, 24, 23);
        plastron.addCuboid(-4F, -1F, -4F, 8, 1, 8);
        plastron.setPivot(0F, yPos, 0F);

        tail = new ModelPart(this, 58, 29);
        tail.addCuboid(0F, 0F, 0F, 1, 1, 2);
        tail.setPivot(-0.5F, yPos - 1, 4F);
    }

    @Override
    public void setAngles(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        head.pitch = headPitch * 0.0174532924F;
        head.yaw = netHeadYaw * 0.0174532924F;

        frontLeftLeg.pitch = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        backLeftLeg.pitch = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        frontRightLeg.pitch = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        backRightLeg.pitch = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

        tail.pitch = 5.934119456780721F;
        tail.yaw = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
    }

    @Override
    public Iterable<ModelPart> getBodyParts() {
        return ImmutableList.of(torso, head, frontLeftLeg, backLeftLeg, frontRightLeg, backRightLeg, plastron, tail);
    }
}
