/*
 * HuntsmanModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import com.google.common.collect.ImmutableList;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.entity.model.CompositeEntityModel;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

@Environment(EnvType.CLIENT)
public class HuntsmanModel<T extends Entity> extends CompositeEntityModel<T> {
    private final ModelPart head;
    private final ModelPart thorax;
    private final ModelPart abdomen;
    private final ModelPart rightBackLeg;
    private final ModelPart leftBackLeg;
    private final ModelPart rightBackMiddleLeg;
    private final ModelPart leftBackMiddleLeg;
    private final ModelPart rightFrontMiddleLeg;
    private final ModelPart leftFrontMiddleLeg;
    private final ModelPart rightFrontLeg;
    private final ModelPart leftFrontLeg;

    public HuntsmanModel() {
        this.head = new ModelPart(this, 32, 4);
        this.head.addCuboid(-4.0F, -4.0F, -8.0F, 8.0F, 8.0F, 8.0F, 0.0F);
        this.head.setPivot(0.0F, 15.0F, -3.0F);
        this.thorax = new ModelPart(this, 0, 0);
        this.thorax.addCuboid(-3.0F, -3.0F, -3.0F, 6.0F, 6.0F, 6.0F, 0.0F);
        this.thorax.setPivot(0.0F, 15.0F, 0.0F);
        this.abdomen = new ModelPart(this, 0, 12);
        this.abdomen.addCuboid(-5.0F, -4.0F, -6.0F, 10.0F, 8.0F, 12.0F, 0.0F);
        this.abdomen.setPivot(0.0F, 15.0F, 9.0F);
        this.rightBackLeg = new ModelPart(this, 18, 0);
        this.rightBackLeg.addCuboid(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        this.rightBackLeg.setPivot(-4.0F, 15.0F, 2.0F);
        this.leftBackLeg = new ModelPart(this, 18, 0);
        this.leftBackLeg.addCuboid(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        this.leftBackLeg.setPivot(4.0F, 15.0F, 2.0F);
        this.rightBackMiddleLeg = new ModelPart(this, 18, 0);
        this.rightBackMiddleLeg.addCuboid(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        this.rightBackMiddleLeg.setPivot(-4.0F, 15.0F, 1.0F);
        this.leftBackMiddleLeg = new ModelPart(this, 18, 0);
        this.leftBackMiddleLeg.addCuboid(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        this.leftBackMiddleLeg.setPivot(4.0F, 15.0F, 1.0F);
        this.rightFrontMiddleLeg = new ModelPart(this, 18, 0);
        this.rightFrontMiddleLeg.addCuboid(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        this.rightFrontMiddleLeg.setPivot(-4.0F, 15.0F, 0.0F);
        this.leftFrontMiddleLeg = new ModelPart(this, 18, 0);
        this.leftFrontMiddleLeg.addCuboid(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        this.leftFrontMiddleLeg.setPivot(4.0F, 15.0F, 0.0F);
        this.rightFrontLeg = new ModelPart(this, 18, 0);
        this.rightFrontLeg.addCuboid(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        this.rightFrontLeg.setPivot(-4.0F, 15.0F, -1.0F);
        this.leftFrontLeg = new ModelPart(this, 18, 0);
        this.leftFrontLeg.addCuboid(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        this.leftFrontLeg.setPivot(4.0F, 15.0F, -1.0F);
    }

    public Iterable<ModelPart> getParts() {
        return ImmutableList.of(this.head, this.thorax, this.abdomen, this.rightBackLeg, this.leftBackLeg, this.rightBackMiddleLeg, this.leftBackMiddleLeg, this.rightFrontMiddleLeg, this.leftFrontMiddleLeg, this.rightFrontLeg, this.leftFrontLeg);
    }

    public void setAngles(T entity, float limbAngle, float limbDistance, float animationProgress, float headYaw, float headPitch) {
        this.head.yaw = headYaw * 0.017453292F;
        this.head.pitch = headPitch * 0.017453292F;
        this.rightBackLeg.roll = -0.7853982F;
        this.leftBackLeg.roll = 0.7853982F;
        this.rightBackMiddleLeg.roll = -0.58119464F;
        this.leftBackMiddleLeg.roll = 0.58119464F;
        this.rightFrontMiddleLeg.roll = -0.58119464F;
        this.leftFrontMiddleLeg.roll = 0.58119464F;
        this.rightFrontLeg.roll = -0.7853982F;
        this.leftFrontLeg.roll = 0.7853982F;
        this.rightBackLeg.yaw = 0.7853982F;
        this.leftBackLeg.yaw = -0.7853982F;
        this.rightBackMiddleLeg.yaw = 0.3926991F;
        this.leftBackMiddleLeg.yaw = -0.3926991F;
        this.rightFrontMiddleLeg.yaw = -0.3926991F;
        this.leftFrontMiddleLeg.yaw = 0.3926991F;
        this.rightFrontLeg.yaw = -0.7853982F;
        this.leftFrontLeg.yaw = 0.7853982F;
        float i = -(MathHelper.cos(limbAngle * 0.6662F * 2.0F + 0.0F) * 0.4F) * limbDistance;
        float j = -(MathHelper.cos(limbAngle * 0.6662F * 2.0F + 3.1415927F) * 0.4F) * limbDistance;
        float k = -(MathHelper.cos(limbAngle * 0.6662F * 2.0F + 1.5707964F) * 0.4F) * limbDistance;
        float l = -(MathHelper.cos(limbAngle * 0.6662F * 2.0F + 4.712389F) * 0.4F) * limbDistance;
        float m = Math.abs(MathHelper.sin(limbAngle * 0.6662F + 0.0F) * 0.4F) * limbDistance;
        float n = Math.abs(MathHelper.sin(limbAngle * 0.6662F + 3.1415927F) * 0.4F) * limbDistance;
        float o = Math.abs(MathHelper.sin(limbAngle * 0.6662F + 1.5707964F) * 0.4F) * limbDistance;
        float p = Math.abs(MathHelper.sin(limbAngle * 0.6662F + 4.712389F) * 0.4F) * limbDistance;
        ModelPart rbl = this.rightBackLeg;
        rbl.yaw += i;
        rbl = this.leftBackLeg;
        rbl.yaw += -i;
        rbl = this.rightBackMiddleLeg;
        rbl.yaw += j;
        rbl = this.leftBackMiddleLeg;
        rbl.yaw += -j;
        rbl = this.rightFrontMiddleLeg;
        rbl.yaw += k;
        rbl = this.leftFrontMiddleLeg;
        rbl.yaw += -k;
        rbl = this.rightFrontLeg;
        rbl.yaw += l;
        rbl = this.leftFrontLeg;
        rbl.yaw += -l;
        rbl = this.rightBackLeg;
        rbl.roll += m;
        rbl = this.leftBackLeg;
        rbl.roll += -m;
        rbl = this.rightBackMiddleLeg;
        rbl.roll += n;
        rbl = this.leftBackMiddleLeg;
        rbl.roll += -n;
        rbl = this.rightFrontMiddleLeg;
        rbl.roll += o;
        rbl = this.leftFrontMiddleLeg;
        rbl.roll += -o;
        rbl = this.rightFrontLeg;
        rbl.roll += p;
        rbl = this.leftFrontLeg;
        rbl.roll += -p;
    }
}
