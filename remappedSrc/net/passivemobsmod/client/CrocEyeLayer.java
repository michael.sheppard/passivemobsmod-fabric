/*
 * CrocEyeLayer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import net.passivemobsmod.common.CrocEntity;
import net.passivemobsmod.common.PassiveMobsMod;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.entity.feature.EyesFeatureRenderer;
import net.minecraft.client.render.entity.feature.FeatureRendererContext;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public class CrocEyeLayer<T extends CrocEntity, M extends CrocModel<T>> extends EyesFeatureRenderer<T, M> {

    private static final RenderLayer EYES = RenderLayer.getEyes(new Identifier(PassiveMobsMod.MODID, "textures/entity/crocodile/croc_eyes.png"));

    public CrocEyeLayer(FeatureRendererContext<T, M> renderer) {
        super(renderer);
    }

    @Override
    public RenderLayer getEyesTexture() {
        return EYES;
    }

}
