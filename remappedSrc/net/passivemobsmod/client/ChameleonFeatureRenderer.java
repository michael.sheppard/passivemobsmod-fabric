/*
 * ChameleonFeatureRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.passivemobsmod.common.ChameleonEntity;
import net.passivemobsmod.common.PassiveMobsMod;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.color.world.BiomeColors;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.feature.FeatureRenderer;
import net.minecraft.client.render.entity.feature.FeatureRendererContext;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

@Environment(EnvType.CLIENT)
public class ChameleonFeatureRenderer extends FeatureRenderer<ChameleonEntity, ChameleonEntityModel<ChameleonEntity>> {
    private static final Identifier TEXTURE = new Identifier(PassiveMobsMod.MODID, "textures/entity/lizards/greyscale_chameleon_p.png");
    private final ChameleonEntityModel<ChameleonEntity> entityModel = new ChameleonEntityModel<>();

    public ChameleonFeatureRenderer(FeatureRendererContext<ChameleonEntity, ChameleonEntityModel<ChameleonEntity>> featureRendererContext) {
        super(featureRendererContext);
    }

    @Override
    public void render(MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, ChameleonEntity entity, float limbAngle, float limbDistance,
                       float tickDelta, float animationProgress, float headYaw, float headPitch) {
        float[] tint = getBlockBiomeColors(entity);
        render(getContextModel(), entityModel, TEXTURE, matrices, vertexConsumers, light, entity, limbAngle, limbDistance, animationProgress,
                headYaw, headPitch, tickDelta, tint[0], tint[1], tint[2]);
    }

    // ripped from the BiomeColors class in ChameleonCreeper mod source and slightly modified to suit the chameleon and FabricMC
    // credit for this chameleon color changing algorithm goes to Nikos creator of the ChameleonCreeper mod
    public static float[] getBlockBiomeColors(LivingEntity chameleon) {
        Vec3d v = chameleon.getPos();
        double entityX = v.x;
        double entityY = v.y;
        double entityZ = v.z;

        int red = 0;
        int green = 0;
        int blue = 0;

        int currColor;
        int blockCount = 27; // 3 * 3 * 3

        for (int x = -1; x <= 1; x++) {
            for (int y = 0; y <= 2; ++y) {
                for (int z = -1; z <= 1; z++) {
                    BlockPos bp = new BlockPos(entityX + x, entityY + y - 0.5, entityZ + z);
                    BlockState iBlockState = chameleon.world.getBlockState(bp);
                    Block block = iBlockState.getBlock();

                    if (chameleon.world.getBlockState(bp).isAir()) {
                        blockCount--;
                        continue;
                    }

                    if (block == Blocks.GRASS || block == Blocks.TALL_GRASS) {
                        currColor = BiomeColors.getGrassColor(chameleon.world, new BlockPos(entityX + x, entityY + y, entityZ + z)); // grass
                    } else if (isLeaves(block)) {
                        currColor = BiomeColors.getFoliageColor(chameleon.world, new BlockPos(entityX + x, entityY + y, entityZ + z)); // foliage
                    } else if (block == Blocks.WATER) {
                        currColor = BiomeColors.getWaterColor(chameleon.world, new BlockPos(entityX + x, entityY + y, entityZ + z)); // water
                    } else {
                        currColor = iBlockState.getMaterial().getColor().color; // other blocks
                    }
                    red += (currColor & 0xFF0000) >> 16;
                    green += (currColor & 0xFF00) >> 8;
                    blue += currColor & 0xFF;
                }
            }
        }

        // default to brown color if the Chameleon is falling through the air (without any blocks near it)
        // also has the benefit of avoiding a divide by zero
        if (blockCount == 0) {
            return new float[]{139, 131, 120};
        }

        red /= blockCount;
        green /= blockCount;
        blue /= blockCount;
        float f = 0.003925686274509801f; // (1.0 / 255.0)

        return new float[] {red * f, green * f, blue  * f};
    }

    private static boolean isLeaves(Block block) {
        return block == Blocks.ACACIA_LEAVES |
                block == Blocks.BIRCH_LEAVES |
                block == Blocks.DARK_OAK_LEAVES |
                block == Blocks.JUNGLE_LEAVES |
                block == Blocks.OAK_LEAVES |
                block == Blocks.SPRUCE_LEAVES;
    }

}
