/*
 * BrownBearModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;


import net.passivemobsmod.common.BrownBearEntity;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.entity.model.QuadrupedEntityModel;

@Environment(EnvType.CLIENT)
public class BrownBearModel<T extends BrownBearEntity> extends QuadrupedEntityModel<T> {

    public BrownBearModel() {
        super(12, 0.0F, true, 16.0F, 4.0F, 2.25F, 2.0F, 24);
        textureWidth = 128;
        textureHeight = 64;
        head = new ModelPart(this, 0, 0);
        head.addCuboid(-3.5F, -3.0F, -3.0F, 7.0F, 7.0F, 7.0F, 0.0F);
        head.setPivot(0.0F, 10.0F, -16.0F);
        head.setTextureOffset(0, 44).addCuboid(-2.5F, 1.0F, -6.0F, 5.0F, 3.0F, 3.0F, 0.0F);
        head.setTextureOffset(26, 0).addCuboid(-4.5F, -4.0F, -1.0F, 2.0F, 2.0F, 1.0F, 0.0F);
        ModelPart headPart = head.setTextureOffset(26, 0);
        headPart.mirror = true;
        headPart.addCuboid(2.5F, -4.0F, -1.0F, 2.0F, 2.0F, 1.0F, 0.0F);
        torso = new ModelPart(this);
        torso.setTextureOffset(0, 19).addCuboid(-5.0F, -13.0F, -7.0F, 14.0F, 14.0F, 11.0F, 0.0F);
        torso.setTextureOffset(39, 0).addCuboid(-4.0F, -25.0F, -7.0F, 12.0F, 12.0F, 10.0F, 0.0F);
        torso.setPivot(-2.0F, 9.0F, 12.0F);
        backRightLeg = new ModelPart(this, 50, 22);
        backRightLeg.addCuboid(-2.0F, 0.0F, -2.0F, 4.0F, 10.0F, 8.0F, 0.0F);
        backRightLeg.setPivot(-3.5F, 14.0F, 6.0F);
        backLeftLeg = new ModelPart(this, 50, 22);
        backLeftLeg.addCuboid(-2.0F, 0.0F, -2.0F, 4.0F, 10.0F, 8.0F, 0.0F);
        backLeftLeg.setPivot(3.5F, 14.0F, 6.0F);
        frontRightLeg = new ModelPart(this, 50, 40);
        frontRightLeg.addCuboid(-2.0F, 0.0F, -2.0F, 4.0F, 10.0F, 6.0F, 0.0F);
        frontRightLeg.setPivot(-2.5F, 14.0F, -7.0F);
        frontLeftLeg = new ModelPart(this, 50, 40);
        frontLeftLeg.addCuboid(-2.0F, 0.0F, -2.0F, 4.0F, 10.0F, 6.0F, 0.0F);
        frontLeftLeg.setPivot(2.5F, 14.0F, -7.0F);
        --backRightLeg.pivotX;
        ++backLeftLeg.pivotX;
        ModelPart leg = backRightLeg;
        leg.pivotZ += 0.0F;
        leg = backLeftLeg;
        leg.pivotZ += 0.0F;
        --frontRightLeg.pivotX;
        ++frontLeftLeg.pivotX;
        --frontRightLeg.pivotZ;
        --frontLeftLeg.pivotZ;
    }

    public void setAngles(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        super.setAngles(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
        float currentAge = ageInTicks - (float) entityIn.age;
        float animationProgress = entityIn.getWarningAnimationProgress(currentAge);
        animationProgress *= animationProgress;
        float negAnimationProgress = 1.0F - animationProgress;
        torso.pitch = ((float) Math.PI / 2F) - animationProgress * (float) Math.PI * 0.35F;
        torso.pivotY = 9.0F * negAnimationProgress + 11.0F * animationProgress;
        frontRightLeg.pivotY = 14.0F * negAnimationProgress - 6.0F * animationProgress;
        frontRightLeg.pivotZ = -8.0F * negAnimationProgress - 4.0F * animationProgress;
        ModelPart part = frontRightLeg;
        part.pitch -= animationProgress * (float) Math.PI * 0.45F;
        frontLeftLeg.pivotY = frontRightLeg.pivotY;
        frontLeftLeg.pivotZ = frontRightLeg.pivotZ;
        part = frontLeftLeg;
        part.pitch -= animationProgress * (float) Math.PI * 0.45F;
        if (child) {
            head.pivotY = 10.0F * negAnimationProgress - 9.0F * animationProgress;
            head.pivotZ = -16.0F * negAnimationProgress - 7.0F * animationProgress;
        } else {
            head.pivotY = 10.0F * negAnimationProgress - 14.0F * animationProgress;
            head.pivotZ = -16.0F * negAnimationProgress - 3.0F * animationProgress;
        }
        part = head;
        part.pitch += animationProgress * (float) Math.PI * 0.15F;
    }
}
