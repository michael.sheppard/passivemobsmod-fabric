/*
 * ChameleonEntityRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.passivemobsmod.common.ChameleonEntity;
import net.passivemobsmod.common.PassiveMobsMod;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.passivemobsmod.common.PassiveMobsMod;

@Environment(EnvType.CLIENT)
public class ChameleonEntityRenderer extends MobEntityRenderer<ChameleonEntity, ChameleonEntityModel<ChameleonEntity>> {
    float scaleFactor = 0.35f;
    private static final Identifier TEXTURE = new Identifier(PassiveMobsMod.MODID, "textures/entity/lizards/greyscale_chameleon_p.png");

    public ChameleonEntityRenderer(EntityRenderDispatcher entityRenderDispatcher) {
        super(entityRenderDispatcher, new ChameleonEntityModel<>(), 0.0f);
        addFeature(new ChameleonFeatureRenderer(this));
        shadowRadius = 0.0f;
    }

    @Override
    protected void scale(ChameleonEntity chameleonEntity, MatrixStack matrixStack, float unknown) {
        matrixStack.scale(scaleFactor, scaleFactor, scaleFactor);
    }

    @Override
    public Identifier getTexture(ChameleonEntity entity) {
        return TEXTURE;
    }
}
