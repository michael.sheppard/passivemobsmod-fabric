/*
 * CrocModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import com.google.common.collect.ImmutableList;
import net.passivemobsmod.common.CrocEntity;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.entity.model.QuadrupedEntityModel;
import net.minecraft.util.math.MathHelper;

@Environment(EnvType.CLIENT)
public class CrocModel<T extends CrocEntity> extends QuadrupedEntityModel<T> {

	private final ModelPart rightScaleRow;
	private final ModelPart leftScaleRow;
	private final ModelPart centerScaleRow;
	private final ModelPart firstTailSegment;
	private final ModelPart lastTailSegment;

	public CrocModel() {
		super(12, 0.0f, false, 120.0F, 0.0F, 9.0F, 6.0F, 120);
		float yPos = 19f;

		head = new ModelPart(this, 50, 0);
		head.addCuboid(-4F, 1F, -14F, 8, 3, 14);
		head.setPivot(0F, yPos - 3, -11F);

		torso = new ModelPart(this, 0, 0);
		torso.addCuboid(-5F, -2F, -11F, 10, 4, 22);
		torso.setPivot(0F, yPos, 0F);

		frontLeftLeg = new ModelPart(this, 50, 0);
		frontLeftLeg.addCuboid(0F, 0F, -2F, 3, 6, 4);
		frontLeftLeg.setPivot(5F, yPos + 1F, -8F);
		frontLeftLeg.roll = 5.497787143782138F;

		backLeftLeg = new ModelPart(this, 50, 0);
		backLeftLeg.addCuboid(0F, 0F, -2F, 3, 6, 4);
		backLeftLeg.setPivot(5F, yPos + 1F, 8F);
		backLeftLeg.roll = 5.497787143782138F;

		frontRightLeg = new ModelPart(this, 50, 0);
		frontRightLeg.mirror = true;
		frontRightLeg.addCuboid(-3F, 0F, -2F, 3, 6, 4);
		frontRightLeg.setPivot(-5F, yPos + 1F, -8F);
		frontRightLeg.roll = 0.7853981633974483F;

		backRightLeg = new ModelPart(this, 50, 0);
		backRightLeg.mirror = true;
		backRightLeg.addCuboid(-3F, 0F, -2F, 3, 6, 4);
		backRightLeg.setPivot(-5F, yPos + 1F, 8F);
		backRightLeg.roll = 0.7853981633974483F;

		rightScaleRow = new ModelPart(this, 0, 10);
		rightScaleRow.addCuboid(3F, -2F, -10F, 0, 2, 20);
		rightScaleRow.setPivot(0F, yPos - 2, 0F);

		leftScaleRow = new ModelPart(this, 0, 10);
		leftScaleRow.addCuboid(-3F, -2F, -10F, 0, 2, 20);
		leftScaleRow.setPivot(0F, yPos - 2, 0F);

		centerScaleRow = new ModelPart(this, 0, 10);
		centerScaleRow.addCuboid(0F, -2F, -10F, 0, 2, 20);
		centerScaleRow.setPivot(0F, yPos - 2, 0F);

		firstTailSegment = new ModelPart(this, 16, 15);
		firstTailSegment.addCuboid(-4F, 1F, 0F, 8, 3, 8);
		firstTailSegment.setPivot(0F, yPos - 3F, 11F);

		lastTailSegment = new ModelPart(this, 16, 14);
		lastTailSegment.addCuboid(-3F, 2F, -5F, 6, 2, 10);
		lastTailSegment.setPivot(0F, yPos - 3F, 24F);
	}

	@Override
	protected Iterable<ModelPart> getHeadParts() {
		return ImmutableList.of(head);
	}

	@Override
	protected Iterable<ModelPart> getBodyParts() {
		return ImmutableList.of(torso, frontLeftLeg, backLeftLeg, frontRightLeg, backRightLeg, firstTailSegment, lastTailSegment, centerScaleRow, rightScaleRow, leftScaleRow);
	}

	@Override
	public void setAngles(T entity, float limbAngle, float limbDistance, float animationProgress, float headYaw, float headPitch) {
		head.pitch = headPitch * 0.017453292F;
		head.yaw = headYaw * 0.017453292F;

		frontLeftLeg.pitch = MathHelper.cos(limbAngle * 0.6662F) * 1.4F * limbDistance;
		backLeftLeg.pitch = MathHelper.cos(limbAngle * 0.6662F + (float) Math.PI) * 1.4F * limbDistance;
		frontRightLeg.pitch = MathHelper.cos(limbAngle * 0.6662F + (float) Math.PI) * 1.4F * limbDistance;
		backRightLeg.pitch = MathHelper.cos(limbAngle * 0.6662F) * 1.4F * limbDistance;

		firstTailSegment.yaw = MathHelper.cos(limbAngle * 0.6662F) * 0.4F * limbDistance;
		lastTailSegment.yaw = MathHelper.sin(limbAngle * 0.6662F) * 0.4F * limbDistance;
	}

}
