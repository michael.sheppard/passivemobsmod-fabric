/*
 * BrownBearRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;


import net.passivemobsmod.common.BrownBearEntity;
import net.passivemobsmod.common.PassiveMobsMod;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public class BrownBearRenderer extends MobEntityRenderer<BrownBearEntity, BrownBearModel<BrownBearEntity>> {
    private static final Identifier BROWN_BEAR_TEXTURES = new Identifier(PassiveMobsMod.MODID, "textures/entity/bear/brownbear.png");
    private static final float SCALE_FACTOR = 1.2f;

    public BrownBearRenderer(EntityRenderDispatcher renderDispatcher) {
        super(renderDispatcher, new BrownBearModel<>(), 0.5F);
    }

    public Identifier getTexture(BrownBearEntity entity) {
        return BROWN_BEAR_TEXTURES;
    }

    protected void scale(BrownBearEntity brownBearEntity, MatrixStack matrixStack, float partialTickTime) {
        matrixStack.scale(SCALE_FACTOR, SCALE_FACTOR, SCALE_FACTOR);
        super.scale(brownBearEntity, matrixStack, partialTickTime);
    }
}
