/*
 * GreenLynxModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;

import com.google.common.collect.ImmutableList;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.entity.model.CompositeEntityModel;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

@Environment(EnvType.CLIENT)
public class GreenLynxModel<T extends Entity> extends CompositeEntityModel<T> {
    private final ModelPart head;
    private final ModelPart thorax;
    private final ModelPart abdomen;
    private final ModelPart rightBackLeg;
    private final ModelPart leftBackLeg;
    private final ModelPart rightBackMiddleLeg;
    private final ModelPart leftBackMiddleLeg;
    private final ModelPart rightFrontMiddleLeg;
    private final ModelPart leftFrontMiddleLeg;
    private final ModelPart rightFrontLeg;
    private final ModelPart leftFrontLeg;

    public GreenLynxModel() {
        head = new ModelPart(this, 32, 4);
        head.addCuboid(-4.0F, -4.0F, -8.0F, 8.0F, 8.0F, 8.0F, 0.0F);
        head.setPivot(0.0F, 15.0F, -3.0F);
        thorax = new ModelPart(this, 0, 0);
        thorax.addCuboid(-3.0F, -3.0F, -3.0F, 6.0F, 6.0F, 6.0F, 0.0F);
        thorax.setPivot(0.0F, 15.0F, 0.0F);
        abdomen = new ModelPart(this, 0, 12);
        abdomen.addCuboid(-5.0F, -4.0F, -6.0F, 10.0F, 8.0F, 12.0F, 0.0F);
        abdomen.setPivot(0.0F, 15.0F, 9.0F);
        rightBackLeg = new ModelPart(this, 18, 0);
        rightBackLeg.addCuboid(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        rightBackLeg.setPivot(-4.0F, 15.0F, 2.0F);
        leftBackLeg = new ModelPart(this, 18, 0);
        leftBackLeg.addCuboid(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        leftBackLeg.setPivot(4.0F, 15.0F, 2.0F);
        rightBackMiddleLeg = new ModelPart(this, 18, 0);
        rightBackMiddleLeg.addCuboid(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        rightBackMiddleLeg.setPivot(-4.0F, 15.0F, 1.0F);
        leftBackMiddleLeg = new ModelPart(this, 18, 0);
        leftBackMiddleLeg.addCuboid(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        leftBackMiddleLeg.setPivot(4.0F, 15.0F, 1.0F);
        rightFrontMiddleLeg = new ModelPart(this, 18, 0);
        rightFrontMiddleLeg.addCuboid(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        rightFrontMiddleLeg.setPivot(-4.0F, 15.0F, 0.0F);
        leftFrontMiddleLeg = new ModelPart(this, 18, 0);
        leftFrontMiddleLeg.addCuboid(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        leftFrontMiddleLeg.setPivot(4.0F, 15.0F, 0.0F);
        rightFrontLeg = new ModelPart(this, 18, 0);
        rightFrontLeg.addCuboid(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        rightFrontLeg.setPivot(-4.0F, 15.0F, -1.0F);
        leftFrontLeg = new ModelPart(this, 18, 0);
        leftFrontLeg.addCuboid(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F);
        leftFrontLeg.setPivot(4.0F, 15.0F, -1.0F);
    }

    public Iterable<ModelPart> getParts() {
        return ImmutableList.of(head, thorax, abdomen, rightBackLeg, leftBackLeg, rightBackMiddleLeg, leftBackMiddleLeg, rightFrontMiddleLeg, leftFrontMiddleLeg, rightFrontLeg, leftFrontLeg);
    }

    public void setAngles(T entity, float limbAngle, float limbDistance, float animationProgress, float headYaw, float headPitch) {
        head.yaw = headYaw * 0.017453292F;
        head.pitch = headPitch * 0.017453292F;
        rightBackLeg.roll = -0.7853982F;
        leftBackLeg.roll = 0.7853982F;
        rightBackMiddleLeg.roll = -0.58119464F;
        leftBackMiddleLeg.roll = 0.58119464F;
        rightFrontMiddleLeg.roll = -0.58119464F;
        leftFrontMiddleLeg.roll = 0.58119464F;
        rightFrontLeg.roll = -0.7853982F;
        leftFrontLeg.roll = 0.7853982F;
        rightBackLeg.yaw = 0.7853982F;
        leftBackLeg.yaw = -0.7853982F;
        rightBackMiddleLeg.yaw = 0.3926991F;
        leftBackMiddleLeg.yaw = -0.3926991F;
        rightFrontMiddleLeg.yaw = -0.3926991F;
        leftFrontMiddleLeg.yaw = 0.3926991F;
        rightFrontLeg.yaw = -0.7853982F;
        leftFrontLeg.yaw = 0.7853982F;
        float i = -(MathHelper.cos(limbAngle * 0.6662F * 2.0F + 0.0F) * 0.4F) * limbDistance;
        float j = -(MathHelper.cos(limbAngle * 0.6662F * 2.0F + 3.1415927F) * 0.4F) * limbDistance;
        float k = -(MathHelper.cos(limbAngle * 0.6662F * 2.0F + 1.5707964F) * 0.4F) * limbDistance;
        float l = -(MathHelper.cos(limbAngle * 0.6662F * 2.0F + 4.712389F) * 0.4F) * limbDistance;
        float m = Math.abs(MathHelper.sin(limbAngle * 0.6662F + 0.0F) * 0.4F) * limbDistance;
        float n = Math.abs(MathHelper.sin(limbAngle * 0.6662F + 3.1415927F) * 0.4F) * limbDistance;
        float o = Math.abs(MathHelper.sin(limbAngle * 0.6662F + 1.5707964F) * 0.4F) * limbDistance;
        float p = Math.abs(MathHelper.sin(limbAngle * 0.6662F + 4.712389F) * 0.4F) * limbDistance;
        ModelPart rbl = rightBackLeg;
        rbl.yaw += i;
        rbl = leftBackLeg;
        rbl.yaw += -i;
        rbl = rightBackMiddleLeg;
        rbl.yaw += j;
        rbl = leftBackMiddleLeg;
        rbl.yaw += -j;
        rbl = rightFrontMiddleLeg;
        rbl.yaw += k;
        rbl = leftFrontMiddleLeg;
        rbl.yaw += -k;
        rbl = rightFrontLeg;
        rbl.yaw += l;
        rbl = leftFrontLeg;
        rbl.yaw += -l;
        rbl = rightBackLeg;
        rbl.roll += m;
        rbl = leftBackLeg;
        rbl.roll += -m;
        rbl = rightBackMiddleLeg;
        rbl.roll += n;
        rbl = leftBackMiddleLeg;
        rbl.roll += -n;
        rbl = rightFrontMiddleLeg;
        rbl.roll += o;
        rbl = leftFrontMiddleLeg;
        rbl.roll += -o;
        rbl = rightFrontLeg;
        rbl.roll += p;
        rbl = leftFrontLeg;
        rbl.roll += -p;
    }
}
