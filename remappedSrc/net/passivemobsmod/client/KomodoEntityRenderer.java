/*
 * KomodoEntityRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;


import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.passivemobsmod.common.KomodoEntity;
import net.passivemobsmod.common.PassiveMobsMod;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public class KomodoEntityRenderer extends MobEntityRenderer<KomodoEntity, KomodoEntityModel<KomodoEntity>> {
    private static final Identifier TEXTURE = new Identifier(PassiveMobsMod.MODID, "textures/entity/lizards/komodo32.png");

    public KomodoEntityRenderer(EntityRenderDispatcher entityRenderDispatcher) {
        super(entityRenderDispatcher, new KomodoEntityModel<>(), 0.0f);
    }

    @Override
    protected void scale(KomodoEntity komodoEntity, MatrixStack matrixStack, float unknown) {
        float scaleFactor = komodoEntity.getScaleFactor();
        matrixStack.scale(scaleFactor, scaleFactor, scaleFactor);
    }

    @Override
    public Identifier getTexture(KomodoEntity entity) {
        return TEXTURE;
    }
}
