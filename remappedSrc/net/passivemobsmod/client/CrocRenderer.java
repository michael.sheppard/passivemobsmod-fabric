/*
 * CrocRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.passivemobsmod.client;


import net.passivemobsmod.common.CrocEntity;
import net.passivemobsmod.common.PassiveMobsMod;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public class CrocRenderer extends MobEntityRenderer<CrocEntity, CrocModel<CrocEntity>> {

    private static final Identifier SKIN = new Identifier(PassiveMobsMod.MODID, "textures/entity/crocodile/croc.png");

    public CrocRenderer(EntityRenderDispatcher entityRenderDispatcher) {
        super(entityRenderDispatcher, new CrocModel<>(), 0.0f);
        addFeature(new CrocEyeLayer<>(this));
    }

    @Override
    protected void scale(CrocEntity crocEntity, MatrixStack matrixStack, float unknown) {
        float scaleFactor = crocEntity.getScaleFactor();
        matrixStack.scale(scaleFactor, scaleFactor, scaleFactor);
    }

    @Override
    public Identifier getTexture(CrocEntity entity) {
        return SKIN;
    }

}
